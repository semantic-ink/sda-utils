/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Project configuration
lazy val root = (project in file(".")).
  enablePlugins(GitVersioning).
  enablePlugins(BuildInfoPlugin).
  settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "ink.semantic.meta",
    buildInfoObject := "SdaUtilsBuildInfo"
  ).
  settings(
    inThisBuild(List(
      organization := "ink.semantic",
      scalaVersion := "2.13.0",
      crossScalaVersions := Seq("2.13.0", "2.12.8")
    )),
    name := "sda-utils",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test,
    libraryDependencies ++= Seq(
      // Scala's standard XML library
      "org.scala-lang.modules" %% "scala-xml" % "1.2.0",
      // JSON library of play framework
      "com.typesafe.play" %% "play-json" % "2.8.0-M3",
      // S3 access library
      "io.minio" % "minio" % "6.0.8",
      // Apache Commons
      "commons-io" % "commons-io" % "2.5",
      "org.apache.commons" % "commons-lang3" % "3.9",
      // CLI parser library
      "com.github.scopt" %% "scopt" % "3.7.1"
    ),
    // Logger stuff
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",
    libraryDependencies += "org.log4s" %% "log4s" % "1.8.2"
  )

// Resolve conflict(s) in library dependencies
dependencyOverrides += "com.google.code.findbugs" % "jsr305" % "3.0.1"

// Config for publishing artifacts
licenses := List(
  ("Apache License, Version 2.0",
    url("https://www.apache.org/licenses/LICENSE-2.0"))
)
homepage := Some(url("http://semantic.ink/"))
publishTo := Some("Bintray API Realm" at "https://api.bintray.com/maven/semanticink/maven-release/sda-utils/;publish=1")
credentials += Credentials(new File("credentials.properties"))
publishMavenStyle := true

// Configure Plugins
git.useGitDescribe := true

// Build fat jar and make it a executable shell script
mainClass in assembly := Some("ink.semantic.CLI")

import sbtassembly.AssemblyPlugin.defaultShellScript

assemblyOption in assembly := (assemblyOption in assembly).value.copy(prependShellScript = Some(defaultShellScript))

assemblyJarName in assembly := "sda"

test in assembly := {}
