
# Setup local test environment

Using the test environment we've prepared for `bitbucket-pipelines.yml`.
You may have to set the correct path to the git-repository.

    docker run -it --rm -v $HOME/.sbt:/root/.sbt -v $HOME/.ivy2:/root/.ivy2 -v $HOME/sda-utils:/root/sda-utils themerius/sda-utils

Within the container you can start the build chain, run tests and services needed by the tests.

Prepare services needed by test suite:

    $ minio server /data &

Execute the tests on every file change:

    $ cd sda-utils
    $ sbt -mem 2048 "~test"

## Personal credentials corpera

Now you can use a personalized corpus with you S3 login credentials.
If nothing like this is provided, the POSIX and Java environment is parsed for (useful) SDAs.

It will be also useful for storing other secrect keys, e.g. for your encrypted SDAs. Thought into the future we may store wallet-seeds for you corpera where you've got authorship.


## Use Case: Die Rechnungen.

Es gibt ein Basis-SDA-Container z.B. mit dem Aussehen und Medadaten die immer gleich sind (Anschrift, Logo etc.) und dann gibt es für jede Rechnung jeweils einen eigenen SDA-Container (Tabelle mit Posten etc.)!
