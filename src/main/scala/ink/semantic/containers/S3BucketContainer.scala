/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.containers

import java.time.Instant
import java.net.URI
import java.io.File
import java.nio.charset.StandardCharsets

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.Stack
import scala.collection.JavaConverters._
import scala.xml.Node
import scala.xml.XML

import org.apache.commons.io.FileUtils

import io.minio.MinioClient

import ink.semantic.SdaUtils
import ink.semantic.models.SDA
import ink.semantic.models.SdaEvent
import ink.semantic.models.SdaCorpus
import ink.semantic.models.DocElem
import ink.semantic.models.dsls.DocElemCitation
import ink.semantic.models.dsls.UriAnnotation
import ink.semantic.formats.SdaOutFormat

object S3BucketContainer {

  private val logger = org.log4s.getLogger

  def extractCredentials(corpus: SdaCorpus) = {
    val des = corpus.pragmatism("s3:credentials").getDocElems
    val credentials = for (de <- des) yield {
      val ep = corpus.semantics("ENDPOINT").get.head.asset
      val ak = corpus.semantics("ACCESS_KEY").get.head.asset
      val sk = corpus.semantics("SECRET_KEY").get.head.asset
      new MinioClient(ep, ak, sk)
    }
    if (credentials.isEmpty) {
      throw new IllegalStateException("There is no S3 endpoint defined!")
    } else {
      credentials.head
    }
  }

  def makeBucket(corpusId: String, credentialsCorpus: SdaCorpus) = {
    val client = S3BucketContainer.extractCredentials(credentialsCorpus)
    if (client.bucketExists(corpusId)) {
      logger.info(s"$corpusId bucket already exsists.")
    } else {
      client.makeBucket(corpusId)
      logger.info(s"Created bucket named $corpusId.")
    }
  }

  def bucketExists(corpusId: String, credentialsCorpus: SdaCorpus) = {
    val client = S3BucketContainer.extractCredentials(credentialsCorpus)
    client.bucketExists(corpusId)
  }

}

class S3BucketContainer(corpusId: String, credentialsCorpus: SdaCorpus, sdas: Seq[SDA] = Nil) extends SdaCorpus {

  private val logger = org.log4s.getLogger

  lazy val client = S3BucketContainer.extractCredentials(credentialsCorpus)

  val state = HashMap.empty[String, HashSet[SDA]]
  val editableSessionEventState = Stack.empty[SdaEvent]

  def addToState(sda: SDA, state: HashMap[String, HashSet[SDA]]) = {
    val deSet = state.getOrElse(sda.si, HashSet.empty[SDA])
    deSet -= SdaUtils.provideFingerprint(sda) // HACK: needed because a record is not overwritten, when already in the set
    deSet += SdaUtils.provideFingerprint(sda)
    state(sda.si) = deSet
  }

  def removeFromState(sda: SDA, state: HashMap[String, HashSet[SDA]]) = {
    val deSet = state.getOrElse(sda.si, HashSet.empty[SDA])
    deSet -= SdaUtils.provideFingerprint(sda)
    state(sda.si) = deSet
  }

  sdas.foreach( sda => addToState(sda, state))

  def getCorpusId = corpusId
  def getCorpusContainerFormat = "s3"

  def event2state(event: SdaEvent, state: HashMap[String, HashSet[SDA]]) = {
    event match {
      case SdaEvent(' ', sda) => addToState(sda, state)
      case SdaEvent('x', sda) => removeFromState(sda, state)
      case _ => ;
    }
  }

  def emit(sdas: Seq[SDA]): SdaCorpus = {
    if(System.getProperty("ink.semantic.feature.genProv") != null) {

      sdas.foreach{ sda =>
        val sdaEvent = SdaEvent(' ', sda)
        editableSessionEventState.push(sdaEvent)
        event2state(sdaEvent, state)
        val provSdas = SdaUtils.generateProvenanceFor(sda)
        provSdas.foreach{ sda =>
          val sdaEvent = SdaEvent(' ', sda)
          editableSessionEventState.push(sdaEvent)
          event2state(sdaEvent, state)
        }
      }

    } else {

      sdas.foreach{ sda =>
        val sdaEvent = SdaEvent(' ', sda)
        editableSessionEventState.push(sdaEvent)
        event2state(sdaEvent, state)
      }

    }
    this
  }

  def revoke(sdas: Seq[SDA]): SdaCorpus = {
    sdas.foreach{ sda =>
      val sdaEvent = SdaEvent('x', sda)
      editableSessionEventState.push(sdaEvent)
      event2state(sdaEvent, state)
    }
    this
  }

  def getAssetAsFile(sda: SDA): File = {

    val targetFile = File.createTempFile(s"${sda.se}-", s".${sda.sy}")
    targetFile.deleteOnExit

    if(sda.hasExternalAsset) {

      val de = getDocElem(sda.si)
        .pragmatism("external-asset")
        .semantics("location")
        .syntax("uri-annotation.json")

      val uriAnnots = de.get.map(_.asset).map(UriAnnotation.apply)
      val relevantUriAnnots = uriAnnots
        .filter(_.uri.getScheme == "s3")
        .filter(_.pivot.pr == sda.pr)
        .filter(_.pivot.se == sda.se)
        .filter(_.pivot.sy == sda.sy)

      for(uriAnnot <- relevantUriAnnots) {
        val bucketName = uriAnnot.uri.getAuthority
        val objectName = uriAnnot.uri.getPath
        // try to load the asset
        try {
          val inputStream = client.getObject(bucketName, objectName)
          // targetFile will be overwritten if it already exists
          FileUtils.copyInputStreamToFile(inputStream, targetFile)
          logger.info(s"Created external asset as tmp file $targetFile for $sda with ${uriAnnot}.")
          inputStream.close
        } catch {
          case e: Exception => logger.error(s"Failed to load external asset for $sda with ${uriAnnot} because of $e")
        }
      }

    } else {

      FileUtils.writeStringToFile(targetFile, sda.asset)
      logger.info(s"Created internal asset as tmp file $targetFile for $sda.")

    }

    return targetFile

  }

  def getDocElem(si: String) = {

    // rebuild docelem from sessions
    val deState = HashMap.empty[String, HashSet[SDA]]
    deState(si) = state.getOrElse(si, HashSet.empty[SDA]).clone

    editableSessionEventState.filter(_.sda.si == si).reverse.foreach { event =>
      event2state(event, deState)
    }

    // can handle epoch namespaces
    val searchedTerm = si.split(".epoch.").head
    val searchedNamespace = si.split(".epoch.").tail

    scala.util.Try {
      val minioItems = client.listObjects(corpusId).asScala
      val filteredMinioItems = minioItems.filter{ item =>
        val objName = item.get.objectName.toLowerCase
        objName.contains(searchedTerm.toLowerCase)
      }.filter{ item =>
        val objDate = item.get.lastModified.toInstant
        if(searchedNamespace.size == 1) {
          objDate.toEpochMilli == searchedNamespace.head.toLong
        } else {
          true
        }
      }
      filteredMinioItems.zipWithIndex.foreach{ item =>

        val s3Item = item._1.get
        val objectName = s3Item.objectName
        val etag = s3Item.etag.replaceAll("\"", "")
        val lastModified = s3Item.lastModified
        val objectSize = s3Item.objectSize.toString

        val filename = objectName.split('/').reverse.head
        val name = filename.split('.').head
        val ext = filename.split('.').reverse.headOption.getOrElse("?")

        val siNamespaced = if(filename.contains(".uuid.")) {
          // we don't have to generate a namespace
          filename
        } else {
          // otherwise we give it a epoch namespace
          val epochTag = s"epoch.${lastModified.toInstant.toEpochMilli}"
          s"${filename}.${epochTag}"
        }

        val contentSda = SDA(siNamespaced, "file", s"content", ext, "")
        deState(si) += contentSda
        deState(si) += SDA(siNamespaced, "file", s"etag", "txt", etag)
        deState(si) += SDA(siNamespaced, "file", s"lastModified", "date", lastModified.toString)
        deState(si) += SDA(siNamespaced, "file", s"objectSize", "long", objectSize)

        deState(si) += SDA(siNamespaced, "skos", "altLabel", "txt", name)

        val uri = new URI("s3", corpusId, s"/$objectName", null, null)
        val uriAnnot = UriAnnotation(contentSda, uri)
        deState(si) += SDA(siNamespaced, "external-asset", "location", uriAnnot.syntax, uriAnnot.asJson)

        val cite = DocElemCitation(si = siNamespaced, pr = "file")
        val index = item._2
        // val index = Integer.toUnsignedString(scala.util.Random.nextInt)
        deState(si) += SDA(si, "list", s"item_${index}", "txt", filename)
        deState(si) += SDA(si, "list", s"item_${index}", cite.syntax, cite.asset)
      }
    }

    new DocElem(deState(si).toSeq)

  }

  def getDocElems = {
    getCurrentState.groupBy(_.si).map(kv => new DocElem(kv._2.toSeq)).toSeq
  }

  def getSession(sessionName: String) = {
    val prefix = sessionName
    val minioItems = client.listObjects(corpusId, prefix).asScala
    minioItems.map(_.get.objectName).foreach{
      case objectName if objectName.contains(".sdaout") => scala.util.Try {
        val sessionStream = client.getObject(corpusId, objectName)
        val sdaEvents = if(objectName.endsWith(".gz")) {
          SdaOutFormat.readCompressed(sessionStream)
        } else {
          SdaOutFormat.read(sessionStream)
        }
        sdaEvents.foreach { sdaEvent =>
          event2state(sdaEvent, state)
        }
        sessionStream.close
      }
      case _ => None
    }
  }

  var currentEditableSessionName = s"sessions/${Instant.now.toEpochMilli}"

  def useEditableSession(sessionName: String) = {

    currentEditableSessionName = sessionName

    val sessionChunkObjects = client.listObjects(getCorpusId, sessionName).asScala.map(_.get.objectName).toList.sorted

    sessionChunkObjects.map{
      case name if name.endsWith(".sdaout") => {
        val sessionStream = client.getObject(corpusId, name)
        val sdaEvents = SdaOutFormat.read(sessionStream)
        sdaEvents.foreach { sdaEvent =>
          // send only newly emited/revoked SDAs to editableSessionEventState!
          event2state(sdaEvent, state)
        }
        sessionStream.close
      }
      case name if name.endsWith(".sdaout.gz") => {
        val sessionStream = client.getObject(corpusId, name)
        val sdaEvents = SdaOutFormat.readCompressed(sessionStream)
        sdaEvents.foreach { sdaEvent =>
          // send only newly emited/revoked SDAs to editableSessionEventState!
          event2state(sdaEvent, state)
        }
        sessionStream.close
      }
      case _ => Nil
    }

  }

  def lsSessions(prefix: String): Seq[String] = {
    client.listObjects(getCorpusId, prefix).asScala
      .map(_.get)
      .filter(_.objectName.contains(".sdaout"))
      .map(_.objectName.replace(".sdaout", "").replace(".gz", ""))
      .toSeq
      .distinct
  }

  def flush = flushSession

  def flushSession = flushSession(currentEditableSessionName)

  def flushSession(sessionName: String): String = {
    val localSessionEvents = editableSessionEventState.reverse
    val now = Instant.now.toEpochMilli
    val sessionChunkName = s"${sessionName}/session_chunk_${now}"
    uploadSession(sessionChunkName, localSessionEvents.toSeq)
    editableSessionEventState.clear
    sessionChunkName
  }

  def uploadSession(sessionName: String, sessionEvents: Seq[SdaEvent]) = {
    val tmpFile = if(sessionEvents.size > 1000) {
      SdaOutFormat.writeCompressed(sessionEvents.toIterator)
    } else {
      SdaOutFormat.write(sessionEvents.toIterator)
    }

    if(sessionEvents.size > 1000) {
      client.putObject(corpusId, s"${sessionName}.sdaout.gz", tmpFile.getAbsolutePath)
    } else {
      client.putObject(corpusId, s"${sessionName}.sdaout", tmpFile.getAbsolutePath)
    }

    tmpFile.delete
  }

  // invoke this when you are in an non-concurrent evnrionment, e.g. as cron job
  def compactation = {

    val compactationState = Stack.empty[SdaEvent]

    val alreadyCompactedSession = client
      .listObjects(getCorpusId, currentEditableSessionName)
      .asScala
      .map(_.get.objectName)
      .filterNot(_.contains("session_chunk"))
      .toList
      .sorted

    alreadyCompactedSession.map{
      case name if name.endsWith(".sdaout") => {
        val sessionStream = client.getObject(corpusId, name)
        compactationState.pushAll(SdaOutFormat.read(sessionStream).toSeq)
        sessionStream.close
      }
      case name if name.endsWith(".sdaout.gz") => {
        val sessionStream = client.getObject(corpusId, name)
        compactationState.pushAll(SdaOutFormat.readCompressed(sessionStream).toSeq)
        sessionStream.close
      }
      case _ => Nil
    }

    val sessionChunkObjects = client
      .listObjects(getCorpusId, currentEditableSessionName)
      .asScala.map(_.get.objectName)
      .filter(_.contains("session_chunk"))
      .toList
      .sorted

    sessionChunkObjects.map{
      case name if name.endsWith(".sdaout") => {
        val sessionStream = client.getObject(corpusId, name)
        compactationState.pushAll(SdaOutFormat.read(sessionStream).toSeq)
        sessionStream.close
      }
      case name if name.endsWith(".sdaout.gz") => {
        val sessionStream = client.getObject(corpusId, name)
        compactationState.pushAll(SdaOutFormat.readCompressed(sessionStream).toSeq)
        sessionStream.close
      }
      case _ => Nil
    }

    // create the consolidated session object
    uploadSession(s"${currentEditableSessionName}/session", compactationState.toSeq)

    // remove the chunks
    sessionChunkObjects.map(objectName => client.removeObject(corpusId, objectName))

  }

  def close = {
    state.clear
    editableSessionEventState.clear
  }

  def sigmatics(si: String): InMemoryContainer = {
    new InMemoryContainer(getCurrentState.filter(_.si == si).toSeq)
  }

  def pragmatism(pr: String): InMemoryContainer = {
    new InMemoryContainer(getCurrentState.filter(_.pr == pr).toSeq)
  }

  def semantics(se: String): InMemoryContainer = {
    new InMemoryContainer(getCurrentState.filter(_.se == se).toSeq)
  }

  def syntax(sy: String): InMemoryContainer = {
    new InMemoryContainer(getCurrentState.filter(_.sy == sy).toSeq)
  }

  def isEmpty = getCurrentState.isEmpty

  def nonEmpty = getCurrentState.nonEmpty

  def length = getCurrentState.length

  def getCurrentState = {
    state.clone.map(_._2).flatten.toSeq
  }

}
