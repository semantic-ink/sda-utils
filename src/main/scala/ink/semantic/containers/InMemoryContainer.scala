/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.containers

import java.io.File
import ink.semantic.SdaUtils
import ink.semantic.models.SDA
import ink.semantic.models.SdaCorpus
import ink.semantic.models.DocElem

class InMemoryContainer(corpusId: String, sdas: Seq[SDA]) extends SdaCorpus {

  def this(sdas: Seq[SDA] = Nil) = this(java.util.UUID.randomUUID().toString, sdas)

  var state: Set[SDA] = sdas.toSet

  def getCorpusId = corpusId
  def getCorpusContainerFormat = "jvm"

  def emit(sdas: Seq[SDA]): SdaCorpus = {
    val fingerprintedSdas = sdas.map(SdaUtils.provideFingerprint)
    state = fingerprintedSdas.toSet ++ state
    if(System.getProperty("ink.semantic.feature.genProv") != null) {
      val provSdas = sdas.map(SdaUtils.generateProvenanceFor).flatten
      state = provSdas.toSet ++ state
    }
    this
  }

  def revoke(sdas: Seq[SDA]): SdaCorpus = {
    state = state -- sdas.map(SdaUtils.provideFingerprint)
    this
  }

  def getDocElem(si: String) = new DocElem(state.filter(_.si == si).toSeq)
  def getDocElems = state.groupBy(_.si).map(kv => new DocElem(kv._2.toSeq)).toSeq

  def getSession(sessionName: String) = None

  def useEditableSession(sessionName: String) = None

  def lsSessions(prefix: String): Seq[String] = {
    Nil
  }

  def getAssetAsFile(sda: SDA): File = new File("")

  def flush = None
  def flushSession = flushSession("no-name")
  def flushSession(sessionName: String) = sessionName
  def compactation = None
  def close = None

  def sigmatics(si: String): InMemoryContainer = {
    new InMemoryContainer(state.filter(_.si == si).toSeq)
  }

  def pragmatism(pr: String): InMemoryContainer = {
    new InMemoryContainer(state.filter(_.pr == pr).toSeq)
  }

  def semantics(se: String): InMemoryContainer = {
    new InMemoryContainer(state.filter(_.se == se).toSeq)
  }

  def syntax(sy: String): InMemoryContainer = {
    new InMemoryContainer(state.filter(_.sy == sy).toSeq)
  }

  def isEmpty = state.isEmpty

  def nonEmpty = state.nonEmpty

  def length = state.toSeq.length

  def getCurrentState = state.toSeq

}
