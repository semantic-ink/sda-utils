/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import scala.collection.JavaConverters._
import scala.util.hashing.MurmurHash3

import ink.semantic.models.SDA
import ink.semantic.models.dsls.TopologyItem
import ink.semantic.models.dsls.UriAnnotation

/*
 * Create for a signal with a specific syntax a fingerprint.
 * This fingerprint has only local ("this system instance") validity.
 * It understands the syntax and may only generate a fingerprint from specific properties of the syntax. For example from a NER tag we are only intested in it's offsets because other properties like prefered names can change.
 */
object FingerprintGenerator {

  private val logger = org.log4s.getLogger

  def determine(sda: SDA): String = {
    sda match {
      case SDA(_, _, _, "topology:item", _, _, _) => topologyItem(sda)
      case SDA(_, "skos", "hiddenLabel", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "altLabel", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "prefLabel", _, _, _, _) => sda.fingerprint // we only want one prefLabel SDA!
      case SDA(_, "skos", "exactMatch", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "closeMatch", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "broadMatch", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "narrowMatch", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "relatedMatch", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "broad", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "narrow", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, "skos", "related", _, _, _, _) => simpleAssetHash(sda)
      case SDA(_, _, _, "uri-annotation.json", _, _, _) => uriAnnotation(sda)
      case SDA(_, _, "index", "txt", _, _, _) => simpleAssetHash(sda)
      case _ => sda.fingerprint
    }
  }

  private def topologyItem(sda: SDA): String = try {
    val ti = TopologyItem.apply(sda.signal)
    val comp1 = ti.childSi
    val comp2 = ti.edition
    val hash = MurmurHash3.seqHash(Seq(comp1, comp2))
    Integer.toHexString(hash)
  } catch {
    case e: Exception => {
      val errorMsg = s"Could not interpret signal properly from $sda"
      logger.error(errorMsg)
      throw new Exception(errorMsg)
    }
  }

  private def simpleAssetHash(sda: SDA): String = {
    val hash = MurmurHash3.stringHash(sda.asset)
    Integer.toHexString(hash)
  }

  private def uriAnnotation(sda: SDA): String = try {
    val annot = UriAnnotation.apply(sda.asset)
    val comp1 = annot.pivot
    val comp2 = annot.uri
    val hash = MurmurHash3.seqHash(Seq(comp1, comp2))
    Integer.toHexString(hash)
  } catch {
    case e: Exception => {
      val errorMsg = s"Could not interpret signal properly from $sda"
      logger.error(errorMsg)
      throw new Exception(errorMsg)
    }
  }

}
