/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import ink.semantic.models.SdaCorpus
import ink.semantic.models.DocElem
import ink.semantic.models.TopologyState
import ink.semantic.models.dsls.TopologyItem
import ink.semantic.docelems.Template

class TopologyProcessor[T](corpus: SdaCorpus, namespace: String, maxDepth: Option[Int] = None) {

  private val logger = org.log4s.getLogger

  def listEditions(si: String, pr: String) = {

    val editions =
      corpus.getDocElem(si).pragmatism(pr).syntax("topology:item")
        .signals.toSeq
        .map(TopologyItem.apply)
        .map(_.edition)

    editions.toSet.toSeq.sorted.reverse

  }

  def defaultEdition(si: String, pr: String): Option[String] = {
    listEditions(si, pr).headOption
  }

  private def retrieveTopologyItems(ts: TopologyState): Seq[TopologyItem] = {
    corpus.getDocElem(ts.si).pragmatism(ts.pr).semantics(ts.se).syntax("topology:item")
      .signals.toSeq
      .map(TopologyItem.apply)
      .sortBy(_.rank)
  }

  private def topologyItems(ts: TopologyState): Seq[TopologyItem] = {

    val items = if(maxDepth.isEmpty) {
      retrieveTopologyItems(ts)
    } else {
      // stop topology recursion when desired depth is reached
      if(maxDepth.get == ts.depth) {
        Nil
      } else {
        retrieveTopologyItems(ts)
      }
    }

    if (ts.edition.nonEmpty) {
      // consider only topology items of the desired edition
      items.filter(_.edition == ts.edition.get)
    } else {
      items
    }

  }

  def getRepresentation(ts: TopologyState): T = {
    try {
      val constructor = Class.forName(s"$namespace.${ts.prAsCamelCase}")
      val instance = constructor.newInstance().asInstanceOf[Template[T]]
      val de = corpus.getDocElem(ts.si)
      instance.render(de, this, ts)
    } catch {
      case e: java.lang.ClassNotFoundException => {
        logger.debug(s"We found no implementation for DocElem pragmatics ${ts.pragmatism}")
        val constructor = Class.forName(s"$namespace.Generic")
        val instance = constructor.newInstance().asInstanceOf[Template[T]]
        val de = corpus.getDocElem(ts.si)
        instance.render(de, this, ts)
      }
    }
  }

  def lowestRank(ts: TopologyState): Int = {
    val items = topologyItems(ts)
    if (items.nonEmpty) {
      items.head.rank
    } else {
      // there is no lowest rank
      0
    }
  }

  def highestRank(ts: TopologyState): Int = {
    val items = topologyItems(ts)
    if (items.nonEmpty) {
      items.last.rank
    } else {
      // there is no highest rank
      0
    }
  }

  def upperSiblingRank(ts: TopologyState): Int = {
    if(ts.superior.nonEmpty) {
      val items = topologyItems(ts.superior.get)
      // find the the current topology item with it's sibling
      val siblings = items.sliding(2).filter(_(0).rank == ts.rank).toList
      if(siblings.nonEmpty) {
        val nextSibling = siblings.head
        if(nextSibling.size == 2) {
          // return the rank of the next sibling
          nextSibling(1).rank
        } else {
          0
        }
      } else {
        // there is no upper sibling
        0
      }
    } else {
      // no superior element!
      0
    }
  }

  private def updateTopologyState(topoItem: TopologyItem, ts: TopologyState) = {
    val updatedTs = TopologyState(topoItem.childSi, topoItem.childPr, topoItem.childEdition, topoItem.rank, ts.depth + 1, ts.semantics, Option(ts))
    getRepresentation(updatedTs)
  }

  def children(implicit ts: TopologyState) = {
    topologyItems(ts).map(updateTopologyState(_, ts))
  }

  def directChildrenDocElems(implicit ts: TopologyState): Seq[DocElem] = {
    topologyItems(ts).map(ti => corpus.getDocElem(ti.childSi))
  }

  private def updateTopologyState_allChildrenDocElems(topoItem: TopologyItem, ts: TopologyState): Seq[DocElem] = {
    val updatedTs = TopologyState(topoItem.childSi, topoItem.childPr, topoItem.childEdition, topoItem.rank, ts.depth + 1, ts.semantics, Option(ts))
    allChildrenDocElems(updatedTs)
  }

  def allChildrenDocElems(implicit ts: TopologyState): Seq[DocElem] = {
    val directChildDocElems = topologyItems(ts).map(ti => corpus.getDocElem(ti.childSi))
    val nextChildDocElems = topologyItems(ts).map(updateTopologyState_allChildrenDocElems(_, ts)).flatten
    directChildDocElems ++ nextChildDocElems
  }

}
