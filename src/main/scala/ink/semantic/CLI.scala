/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import ink.semantic.meta.SdaUtilsBuildInfo
import ink.semantic.models.TopologyState

object CLI {

  case class Config(corpusId: String = "", cmd: String = "")

  val parser = new scopt.OptionParser[Config]("sda") {
    head(SdaUtilsBuildInfo.name, "version", SdaUtilsBuildInfo.version)

    opt[String]('c', "corpus")
      .required()
      .valueName("<id>").action( (id, cfg) =>
      cfg.copy(corpusId = id) ).text("corpus-id is a unique string")

    // TODO tranform/export-format, -as xml or so

    note("\n")

    cmd("ls")
      .action( (_, c) => c.copy(cmd = "ls") )
      .text("list all SDAs in corpus")

    cmd("pp")
      .action( (_, c) => c.copy(cmd = "pp") )
      .text("pretty prints document hierarchy based on the latest topology")

    checkConfig( cfg =>
      if (cfg.cmd.isEmpty) failure("Command cannot be empty")
      else success )
  }

  // export EP_SDA="sh|s3:credentials|ENDPOINT|string|http://objects.semantic.ink"
  // export AK_SDA="sh|s3:credentials|ACCESS_KEY|string|PH7D8H5FQSNRFUUFGYMU"
  // export SK_SDA="sh|s3:credentials|SECRET_KEY|string|rtUX4wfH4tX4y5y9o2RmdbRTOyYJt9HvIJsN3sol"

  def main(args: Array[String]) = {

    parser.parse(args, Config()) match {
      case Some(config) => {
        config match {
          case Config(corpusId, "ls") => {
            val sc = SdaUtils.getSdaCorpus(corpusId)
            // TODO check credentials
            sc.ls.foreach(println)
          }
          case Config(corpusId, "pp") => {
            val sc = SdaUtils.getSdaCorpus(corpusId)
            val tp = new TopologyProcessor[String](sc, "ink.semantic.docelems.cli")
            val edition = tp.defaultEdition(corpusId, "header")
            val ts = TopologyState(corpusId, "header", edition)
            println(tp.getRepresentation(ts))
          }
          case other => {
            println(s"Can't handle $other")
          }
        }

      }
      case None => {
        // arguments are bad, error message will have been displayed
      }
    }

  }


}
