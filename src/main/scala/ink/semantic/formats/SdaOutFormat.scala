/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.formats

import java.io.InputStream
import java.io.File
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils

import ink.semantic.models.SDA
import ink.semantic.models.SdaEvent


object SdaOutFormat {

  def read(stream: InputStream): Iterator[SdaEvent] = {
    new SdaOutIterator(stream)
  }

  def readCompressed(stream: InputStream): Iterator[SdaEvent] = {
    val gzipStream = new GZIPInputStream(stream, 65536)
    new SdaOutIterator(gzipStream)
  }

  def write(state: Iterator[SdaEvent]): File = {
    val targetFile = File.createTempFile("session", ".sdaout")

    state.foreach{event =>
      val repr = SdaOutFormat.mkFormat(event)
      val encoding = StandardCharsets.UTF_8
      val append = true
      FileUtils.writeStringToFile(targetFile, repr, encoding, append)
    }

    targetFile.deleteOnExit
    targetFile
  }

  def writeCompressed(state: Iterator[SdaEvent]): File = {
    val targetFile = File.createTempFile("session", ".sdaout.gz")
    val fileStream = new FileOutputStream(targetFile)
    val gzipStream = new GZIPOutputStream(fileStream)

    state.foreach{event =>
      val repr = SdaOutFormat.mkFormat(event)
      gzipStream.write(repr.getBytes)
    }

    gzipStream.close
    fileStream.close

    targetFile.deleteOnExit
    targetFile
  }

  def mkFormat(sdaEvent: SdaEvent): String = {

    val serialized = StringBuilder.newBuilder

    serialized ++= SdaOutFormatUtils.mask(sdaEvent.sda.si)
    serialized += '\t'
    serialized ++= SdaOutFormatUtils.mask(sdaEvent.sda.pr)
    serialized += '\t'
    serialized ++= SdaOutFormatUtils.mask(sdaEvent.sda.se)
    serialized += '\t'
    serialized ++= SdaOutFormatUtils.mask(sdaEvent.sda.sy)
    serialized += '\t'
    serialized += sdaEvent.event
    serialized += '\t'
    serialized ++= SdaOutFormatUtils.mask(sdaEvent.sda.as)
    serialized += '\n'

    serialized.toString

  }

}

object SdaOutFormatUtils {

  val endOfLine = '\n'
  val symbolForEndOfLine = '␊'

  val carriageReturn = '\r'
  val symbolForCarriageReturn = '␍'

  val horizontalTabulation = '\t'
  val symbolForHorizontalTabulation = '␉'

  def mask(input: String) = {
    input.map{
      case char if (char == endOfLine) => symbolForEndOfLine
      case char if (char == carriageReturn) => symbolForCarriageReturn
      case char if (char == horizontalTabulation) => symbolForHorizontalTabulation
      case char => char
    }
  }

  def unmask(input: String) = {
    input.map{
      case char if (char == symbolForEndOfLine) => endOfLine
      case char if (char == symbolForCarriageReturn) => carriageReturn
      case char if (char == symbolForHorizontalTabulation) => horizontalTabulation
      case char => char
    }
  }

}

class SdaOutIterator(stream: InputStream) extends Iterator[SdaEvent] {

  val emptySda = SDA.empty
  val lines = IOUtils.lineIterator(stream, StandardCharsets.UTF_8)

  def hasNext = lines.hasNext

  def next = {
    // construct sda event
    StringUtils.split(lines.next, '\t') match {

      case Array(si, pr, se, sy, event, as) => {
        val sigmatics = SdaOutFormatUtils.unmask(si)
        val pragmatism = SdaOutFormatUtils.unmask(pr)
        val semantics = SdaOutFormatUtils.unmask(se)
        val syntax = SdaOutFormatUtils.unmask(sy)
        val eventSymbol = event.head
        val asset = SdaOutFormatUtils.unmask(as)
        SdaEvent(eventSymbol, SDA(sigmatics, pragmatism, semantics, syntax, asset, "", 0L))
      }

      case Array(si, pr, se, sy, event) => {
        val sigmatics = SdaOutFormatUtils.unmask(si)
        val pragmatism = SdaOutFormatUtils.unmask(pr)
        val semantics = SdaOutFormatUtils.unmask(se)
        val syntax = SdaOutFormatUtils.unmask(sy)
        val eventSymbol = event.head
        SdaEvent(eventSymbol, SDA(sigmatics, pragmatism, semantics, syntax, "", "", 0L))
      }

      case _ => SdaEvent('?', emptySda)

    }

  }

}
