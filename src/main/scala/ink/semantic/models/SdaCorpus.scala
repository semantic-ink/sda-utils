/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

import java.lang.AutoCloseable
import java.io.Flushable
import java.nio.file.Path
import java.io.File

// TODO:
// * absorb/merge (merge mit anderem sda container)
// * tmp (not flushed, wird nur zur prozessierungszeit gebraucht/verbraucht)
// * transform (format ändern sqlite->xml)

trait ISdaCorpus extends AutoCloseable with Flushable {

  // Unique name (or hash) of the corpus
  def getCorpusId: String

  // Data format of the container where the corpus is deposited
  def getCorpusContainerFormat: String

  // put SDA to the corpus
  def emit(si: String, pr: String, se: String, sy: String, as: String): SdaCorpus
  def emit(sda: SDA): SdaCorpus
  def emit(sdas: Seq[SDA]): SdaCorpus

  // remove SDA from the corpus
  def revoke(si: String, pr: String, se: String, sy: String, as: String): SdaCorpus
  def revoke(sda: SDA): SdaCorpus
  def revoke(sdas: Seq[SDA]): SdaCorpus

  // Some SDAs may have the acutal asset externally (e.g. images)
  def getAssetAsFile(sda: SDA): File

  // Flushable: flush resources in such a way that the corpus should published to a endpoint!
  // TODO: or should flush produce a immutable version of the SDA-Corpus/Container?!
  // AutoCloseable: close resources so they free filehandlers/ports/threads

  // get the corpus of this session and integrate those SDAs
  def getSession(sessionName: String): Unit
  def useEditableSession(sessionName: String): Unit
  def lsSessions(prefix: String): Seq[String]

  // flush the outcome of this session, returns session name
  def flushSession: String
  def flushSession(sessionName: String): String

  // synchronize the flushes and other cleanup tasks
  def compactation: Unit

}

trait SdaCorpus extends ISdaCorpus with QueryApi {

  def emit(si: String, pr: String, se: String, sy: String, as: String): SdaCorpus = {
    emit(SDA(si, pr, se, sy, as))
  }

  def emit(sda: SDA): SdaCorpus = {
    emit(Seq(sda))
  }

  def revoke(si: String, pr: String, se: String, sy: String, as: String): SdaCorpus = {
    revoke(SDA(si, pr, se, sy, as))
  }

  def revoke(sda: SDA): SdaCorpus = {
    revoke(Seq(sda))
  }

}
