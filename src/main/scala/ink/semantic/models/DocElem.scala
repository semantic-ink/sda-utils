/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

class DocElem(state: Seq[SDA]) extends QueryApi {

  def this(corpus: SdaCorpus) = this(corpus.getCurrentState)

  lazy val sigmatics: Set[String] = {
    state.map(_.si).toSet
  }

  def signals = state.map(_.asset)

  def latestTimestamp = state.sortBy(_.timestamp).reverse.headOption.getOrElse(SDA.empty).timestamp

  def sigmatics(si: String): DocElem = {
    this
  }

  def pragmatism(pr: String): DocElem = {
    if (pr.isEmpty) {
      this
    } else {
      new DocElem(state.filter(_.pr == pr))
    }
  }

  def semantics(se: String): DocElem = {
    if (se.isEmpty) {
      this
    } else {
      new DocElem(state.filter(_.se == se))
    }
  }

  def syntax(sy: String): DocElem = {
    if (sy.isEmpty) {
      this
    } else {
      new DocElem(state.filter(_.sy == sy))
    }
  }

  def isEmpty = state.isEmpty
  def nonEmpty = state.nonEmpty
  def length = state.length
  def getCurrentState = state

  def getDocElem(si: String) = this
  def getDocElems = Seq(this)

  override def toString = s"""DocElem[${sigmatics.mkString("|")}]:${ls.map(_.se).mkString(";")}"""

  // TODO include validate/self-check for docelem, if all sigmatics are actual from the same docelem (lookup proper semantics and so on)...

}
