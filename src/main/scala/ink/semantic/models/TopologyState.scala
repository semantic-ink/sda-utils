/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

case class TopologyState(sigmatics: String, pragmatism: String, edition: Option[String], rank: Int = 0, depth: Int = 0, semantics: String = "child", superior: Option[TopologyState] = None) {
  // The topology knows this document element under thsi sigmatics.
  def si = sigmatics
  // This pragmatism is currenlty relevant for the template (on empty string all pragmatisms are used).
  def pr(pr: String) = this.copy(pragmatism = pr)
  def pr = pragmatism
  // These semantics is used to which part of the topology is follwed (default child).
  // For example we may have an "abstract" semantics with "topology:item" syntax, so we can reuse this part of the topology.
  def follows(se: String) = this.copy(semantics = se)
  def se(se: String) = follows(se)
  def se = semantics

  // The edition denotes which topology is generated.
  // The current recursion depths is also tracked. This is esp useful to render idention.

  def prAsCamelCase = toCamelCase(pragmatism)
  def toCamelCase(name: String) = {
    val n1 = name.head.toString.toUpperCase + name.tail
    val n2 = "\\p{P}([a-z\\d])".r.replaceAllIn(n1, {m =>
      m.group(1).toUpperCase()
    })
    n2
  }
}
