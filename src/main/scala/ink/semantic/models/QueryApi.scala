/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

trait QueryApi {
  def sigmatics(si: String): QueryApi
  def pragmatism(pr: String): QueryApi
  def semantics(se: String): QueryApi
  def syntax(sy: String): QueryApi
  def isEmpty: Boolean
  def nonEmpty: Boolean
  def length: Int
  // returns a set of current state of the SDAs (newest version)
  def getCurrentState: Seq[SDA]
  def get: Seq[SDA] = getCurrentState
  // returns a simple list of all SDAs
  def ls: Seq[SDA] = getCurrentState
  // provide document element
  def getDocElem(sigmatics: String): DocElem
  def getDocElems: Seq[DocElem]
}
