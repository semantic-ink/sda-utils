/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models.dsls

import java.net.URI
import ink.semantic.models.SDA

import play.api.libs.json.Json
import play.api.libs.json.JsValue

case class UriAnnotation(pivot: SDA, uri: URI) {

  // ignore the asset, only semiotic properties are from interest
  // thus it is only referencing?!

  val syntax = "uri-annotation.json"

  def asJson: String = {
    val json = Json.obj(
      "pivot" -> Json.obj(
        "pr" -> pivot.pr,
        "se" -> pivot.se,
        "sy" -> pivot.sy,
      ),
      "uri" -> uri.toString
    )
    Json.stringify(json)
  }

}

object UriAnnotation {

  def apply(dsl: String): UriAnnotation = UriAnnotation.apply(Json.parse(dsl))

  def apply(json: JsValue): UriAnnotation = {
    val pivot = SDA(
      "",
      (json \ "pivot" \ "pr").as[String],
      (json \ "pivot" \ "se").as[String],
      (json \ "pivot" \ "sy").as[String],
      ""
    )
    val uri = new URI((json \ "uri").as[String])
    UriAnnotation(pivot, uri)
  }

}
