/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models.dsls

import play.api.libs.json.Json
import play.api.libs.json.JsValue

case class TopologyItem(edition: String, rank: Int, childSi: String, childPr: String, childEdition: Option[String]) {

  def asJson: String = {
    val json = Json.obj(
      "edition" -> edition,
      "rank" -> rank,
      if(childEdition.nonEmpty) {
        "child" -> Json.obj(
          "si" -> childSi,
          "pr" -> childPr,
          "edition" -> childEdition
        )
      } else {
        "child" -> Json.obj(
          "si" -> childSi,
          "pr" -> childPr
        )
      }
    )
    Json.stringify(json)
  }

}

object TopologyItem {

  def apply(dsl: String): TopologyItem = TopologyItem.apply(Json.parse(dsl))

  def apply(json: JsValue): TopologyItem = {
    val edition = (json \ "edition").as[String]
    val rank = (json \ "rank").as[Int]
    val childSi = (json \ "child" \ "si").as[String]
    val childPr = (json \ "child" \ "pr").as[String]
    val childEdition = (json \ "child" \ "edition").asOpt[String]
    TopologyItem(edition, rank, childSi, childPr, childEdition)
  }

}
