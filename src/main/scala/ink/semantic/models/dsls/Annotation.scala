/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models.dsls

import ink.semantic.models.SDA

import play.api.libs.json.Json
import play.api.libs.json.JsValue

case class Annotation(comment: SDA, position: SDA, coordinates: Seq[Int]) {

  // ignore the asset, only semiotic properties are from interest
  // thus it is only referencing?!

  def syntax = "offset-annotation.json"

  def asset = asJson

  def asJson: String = {
    val json = Json.obj(
      "comment" -> Json.obj(
        "si" -> comment.si,
        "pr" -> comment.pr,
        "se" -> comment.se,
        "sy" -> comment.sy,
        "fp" -> comment.fp
      ),
      "position" -> Json.obj(
        "si" -> position.si,
        "pr" -> position.pr,
        "se" -> position.se,
        "sy" -> position.sy,
        "fp" -> position.fp,
        "coordinates" -> coordinates  // for navigation within the asset/dsl
      )
    )
    Json.stringify(json)
  }

}

object Annotation {

  def apply(dsl: String): Annotation = Annotation.apply(Json.parse(dsl))

  def apply(json: JsValue): Annotation = {
    val comment = SDA(
      (json \ "comment" \ "si").as[String],
      (json \ "comment" \ "pr").as[String],
      (json \ "comment" \ "se").as[String],
      (json \ "comment" \ "sy").as[String],
      "",
      (json \ "comment" \ "fp").as[String]
    )
    val position = SDA(
      (json \ "position" \ "si").as[String],
      (json \ "position" \ "pr").as[String],
      (json \ "position" \ "se").as[String],
      (json \ "position" \ "sy").as[String],
      "",
      (json \ "position" \ "fp").as[String]
    )
    val coordinates = (json \ "position" \ "coordinates").as[Seq[Int]]
    Annotation(comment, position, coordinates)
  }

}
