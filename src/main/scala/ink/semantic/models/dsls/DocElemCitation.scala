/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models.dsls

import ink.semantic.models.SDA

import play.api.libs.json.Json
import play.api.libs.json.JsValue

case class DocElemCitation(si: String, pr: String) {

  // ignore the asset, only semiotic properties are from interest
  // thus it is only referencing?!

  def syntax = "docelem-citation.json"

  def asset = asJson

  def asJson: String = {
    val json = Json.obj(
      "si" -> si,
      "pr" -> pr
    )
    Json.stringify(json)
  }

}

object DocElemCitation {

  def apply(dsl: String): DocElemCitation = DocElemCitation.apply(Json.parse(dsl))

  def apply(json: JsValue): DocElemCitation = {
    val si = (json \ "si").as[String]
    val pr = (json \ "pr").as[String]
    DocElemCitation(si, pr)
  }

}
