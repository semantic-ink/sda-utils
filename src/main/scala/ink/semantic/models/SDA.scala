/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

import scala.util.hashing.MurmurHash3
import scala.math.Ordered.orderingToOrdered


object SDA {
  def apply(sigmatics: String, pragmatism: String, semantics: String, syntax: String, asset: String, fingerprint: String) = new SDA(sigmatics, pragmatism, semantics, syntax, asset, fingerprint)
  def apply(sigmatics: String, pragmatism: String, semantics: String, syntax: String, asset: String) = new SDA(sigmatics, pragmatism, semantics, syntax, asset)
  val empty = new SDA("", "", "", "", "")
}

case class SDA(
  sigmatics: String,
  pragmatism: String,
  semantics: String,
  syntax: String,
  asset: String,
  fingerprint: String,
  editstamp: Long
) extends Ordered[SDA] {
  def this(sigmatics: String, pragmatism: String, semantics: String, syntax: String, asset: String, fingerprint: String) = this(sigmatics, pragmatism, semantics, syntax, asset, fingerprint, System.nanoTime)
  def this(sigmatics: String, pragmatism: String, semantics: String, syntax: String, asset: String) = this(sigmatics, pragmatism, semantics, syntax, asset, "")
  def si = sigmatics
  def getSi = sigmatics
  def pr = pragmatism
  def getPr = pragmatism
  def se = semantics
  def getSe = semantics
  def sy = syntax
  def getSy = syntax
  def fp = fingerprint
  def getFp = fingerprint
  def as = asset
  def signal = asset
  def getSignal = asset
  def getAsset = asset
  def eid = editstamp
  def ts = editstamp
  def timestamp = editstamp
  def getEditId = editstamp
  def hasExternalAsset = this.asset.isEmpty
  def toXML = <sda si={sigmatics} pr={pragmatism} se={semantics} sy={syntax} fp={fingerprint} ts={editstamp.toString}>{scala.xml.PCData(asset)}</sda>
  override def hashCode: Int = {
    val h1 = MurmurHash3.stringHash(sigmatics, MurmurHash3.stringSeed)
    val h2 = MurmurHash3.stringHash(pragmatism, h1)
    val h3 = MurmurHash3.stringHash(semantics, h2)
    val h4 = MurmurHash3.stringHash(syntax, h3)
    MurmurHash3.stringHash(fingerprint, h4)
  }
  override def equals(that: Any): Boolean = that match {
    case that: SDA => that.canEqual(this) && this.hashCode == that.hashCode
    case _ => false
  }
  def compare(that: SDA): Int = (this.sigmatics, this.editstamp) compare (that.sigmatics, that.editstamp)
}
