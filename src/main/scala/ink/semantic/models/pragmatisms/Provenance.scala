package ink.semantic.models.pragmatisms

import java.time.Instant
import ink.semantic.SdaUtils
import ink.semantic.models.SDA
import ink.semantic.models.dsls.SdaCitation

case class Provenance(pivot: SDA, annotatorName: String, annotatorVersion: String, time: Instant) {

  val hash = SdaUtils.sdaId(pivot)
  // val entity = SdaCitation(pivot)

  def mkSdas = List(
    SDA(pivot.si, "provenance", s"index", "txt", hash.toString),
    // SDA(pivot.si, "provenance", s"entity_$hash", entity.syntax, entity.asset),
    SDA(pivot.si, "provenance", s"entity-si_$hash", "txt", pivot.si),
    SDA(pivot.si, "provenance", s"entity-pr_$hash", "txt", pivot.pr),
    SDA(pivot.si, "provenance", s"entity-se_$hash", "txt", pivot.se),
    SDA(pivot.si, "provenance", s"entity-sy_$hash", "txt", pivot.sy),
    SDA(pivot.si, "provenance", s"entity-hash_$hash", "txt", hash),
    SDA(pivot.si, "provenance", s"annotator-name_$hash", "txt", annotatorName),
    SDA(pivot.si, "provenance", s"annotator-version_$hash", "txt", annotatorVersion),
    SDA(pivot.si, "provenance", s"time_$hash", "iso8601.txt", time.toString)
  )

}
