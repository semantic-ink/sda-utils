/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import scala.collection.JavaConverters._
import ink.semantic.models.SDA
import ink.semantic.models.SdaCorpus
import ink.semantic.models.dsls.TopologyItem
import ink.semantic.models.dsls.Annotation
import ink.semantic.containers.InMemoryContainer
import ink.semantic.containers.S3BucketContainer
import org.joda.time.DateTime

import play.api.libs.json.Json

import java.util.Base64
import scala.collection.immutable.BitSet
import scala.xml.Node
import scala.util.hashing.MurmurHash3

object SdaUtils {

  private val logger = org.log4s.getLogger

  // just generate a empty in-memory corpus.
  def getSdaCorpus: SdaCorpus = {
    new InMemoryContainer(Nil)
  }

  // get credentials implicitly from environment or create as fallback a in-memory corpus.
  def getSdaCorpus(corpusId: String): SdaCorpus = {
    val cred = SdaUtils.environment
    if(cred.nonEmpty) {
      getSdaCorpus(corpusId, cred)
    } else {
      new InMemoryContainer(corpusId, Nil)
    }
  }

  // provide credentials explicitly or create as fallback a in-memory corpus.
  def getSdaCorpus(corpusId: String, credentials: SdaCorpus): SdaCorpus = {
    try {
      S3BucketContainer.makeBucket(corpusId, credentials)
      new S3BucketContainer(corpusId, credentials)
    } catch {
      case e: Exception => {
        logger.error(s"Could not create S3SdaCorpus, creating instead a InMemoryCorpus. Cause: $e")
        new InMemoryContainer(corpusId, Nil)
      }
    }
  }

  def sdaCorpusExists(corpusId: String, credentials: SdaCorpus): Boolean = {
    S3BucketContainer.bucketExists(corpusId, credentials)
  }

  def env2sda(name: String, envname: String)(getenv: String => String = System.getenv): SDA = {
    val env = Option(getenv(name))
    if (env.isDefined) {
      if (name.endsWith("_SDA") || name.endsWith("_sda")) {
        // parse SDA
        env.get.split("\\|") match {
          case Array(si, pr, se, sy, as) => SDA(si, pr, se, sy, as)
          case _ => SDA.empty
        }
      } else {
        // create generic SDA
        val user = System.getProperty("user.name")
        val hostname = java.net.InetAddress.getLocalHost.getHostName
        SDA(s"$user@$hostname", s"$envname:environment-variable", name, "string", env.get)
      }
    } else {
      SDA.empty
    }
  }

  def pullSystemEnv: Seq[SDA] = {
    System.getenv.keySet.asScala.toSeq.map(name => env2sda(name, "posix")())
  }

  def pullJavaPropertyEnv: Seq[SDA] = {
    System.getProperties.keySet.asScala.map(_.toString).toSeq.map(name => env2sda(name, "java")(System.getProperty))
  }

  def pullEnv: Seq[SDA] = pullSystemEnv ++ pullJavaPropertyEnv

  def environment: SdaCorpus = new InMemoryContainer(SdaUtils.pullEnv)

  lazy val defaultEdition = {
    // Generate a day fine edtion when this value is used
    // Use Case: If a importer tool automatically decomposes a document into SDAs. If a re-import is done, a new edition is generated or when used on the same day the latest topology annotation "wins" (useful on development of such a importer component). Note: manual created edtions should be the in form "date:tag" then?
    DateTime.now.toString("yyyy-MM-dd")
  }

  def link(parent: SDA, child: SDA, rank: Int, edition: String): SDA = {
    val ti = TopologyItem(edition, rank, child.si, child.pr, Option.empty)
    val sda = SDA(parent.si, parent.pr, "child", "topology:item", ti.asJson)
    provideFingerprint(sda)
  }

  def link(parent: SDA, child: SDA, rank: Int): SDA = link(parent, child, rank, defaultEdition)

  def annotate(thing: SDA, coordinates: Seq[Int], comment: SDA, annotationPr: String, annotationSe: String): SDA = {
    val annot = Annotation(comment, thing, coordinates)
    SDA(thing.si, annotationPr, annotationSe, "annotation", annot.asJson)
  }

  // TODO: link: it is possible to wire a semantics other than child (e.g. a header|abstract) with a topology. Use case: re-use the abstract text. -> Introduce a more complex link function

  def provideFingerprint(sda: SDA): SDA = {
    // A SDA creator has to be aware, if his SDA should be fingerprinted or not! And if this library did not have any defaults on that syntax, he has to add it manually! (It is important to use the same algorithms to create a specific fingerprint...)
    // The creator could also use the FingerprintGenerator direclty (or extend it)
    val fingerprint = FingerprintGenerator.determine(sda)
    sda.copy(fingerprint = fingerprint)
  }

  def generateSimpleSentenceVector(str: String) = {
    // remove all Punctuation and Control characters
    val pattern = "\\p{P}|\\p{C}".r
    val cleanStr = pattern.replaceAllIn(str, "").toUpperCase
    // split at Separator characters
    val splitPattern = "\\p{Z}".r
    val tokens = splitPattern.replaceAllIn(cleanStr, " ").split(" ")
    val edgeTrigrams = tokens.map(_.sliding(3).toList).toList.flatten
    // upper case creates smaller integers
    // in a text there should be no control characters, so we begin at the zero character to count
    val edgeTrigramSums = edgeTrigrams.map(_.sum - "000".sum).filter(_ >= 0)
    val b1 = BitSet(edgeTrigramSums:_*)
    val bmask = b1.toBitMask
    // transform the bit mask to an byte array and compress it further
    val barr = bmask.map(BigInt(_).toByteArray).flatten.toSet.toArray
    // create a more "readable" byte array/vector
    Base64.getEncoder.encode(barr)
  }

  def addSimpleSentenceHash(sda: SDA): SDA = sda match {
    case SDA(_, "sentence", _, _, asset, _, _) => sda.copy(sigmatics = new String(generateSimpleSentenceVector(asset)))
    case nonSentence => nonSentence
  }

  def mkXml(corpusId: String, sdas: Seq[SDA]): Node = {
    <corpus xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://semantic.ink/sda http://semantic.ink/assets/SDACorpus.xsd" xmlns="http://semantic.ink/sda" root={corpusId}>
      {sdas.sorted.reverse.toSet[SDA].map(_.toXML)}
    </corpus>
  }

  def mkSdas(xmlObj: Node): Seq[SDA] = {
    for (xsda <- (xmlObj \"sda")) yield {
      val si = (xsda \ "@si").text
      val pr = (xsda \ "@pr").text
      val se = (xsda \ "@se").text
      val sy = (xsda \ "@sy").text
      val signal = xsda.text
      val fingerprint = (xsda \ "@fp").text
      val timestamp = (xsda \ "@ts").text.toLong
      val sda = SDA(si, pr, se, sy, signal, fingerprint, timestamp)
      SdaUtils.provideFingerprint(sda)
    }
  }

  def mkXmlString(xmlObj: Node): String = {
    s"""|<?xml version="1.0" encoding="UTF-8"?>
        |${xmlObj}""".stripMargin
  }

  def sdaId(sda: SDA): String = {
    val hash = MurmurHash3.seqHash(Seq(sda.si, sda.pr, sda.se, sda.sy, sda.as))
    Integer.toUnsignedString(hash)
  }

  def getAnnotatorName: String = {
    val name = ink.semantic.meta.SdaUtilsBuildInfo.name
    System.getenv().getOrDefault("ANNOTATOR_NAME", name)
  }

  def getAnnotatorVersion: String = {
    val version = ink.semantic.meta.SdaUtilsBuildInfo.version
    System.getenv().getOrDefault("ANNOTATOR_VERSION", version)
  }

  def generateProvenanceFor(pivot: SDA): Seq[SDA] = {
    val prov = ink.semantic.models.pragmatisms.Provenance(pivot, SdaUtils.getAnnotatorName, SdaUtils.getAnnotatorVersion, java.time.Instant.now)
    prov.mkSdas
  }

}
