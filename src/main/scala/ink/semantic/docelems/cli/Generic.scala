/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.docelems.cli

import ink.semantic.TopologyProcessor
import ink.semantic.docelems.Template
import ink.semantic.models.DocElem
import ink.semantic.models.TopologyState

class Header extends Template[String] {
  def render(de: DocElem, tp: TopologyProcessor[String], ts: TopologyState): String = {
    s"""
      |-- Front Matter --
      |Title:
      |  ${de.pragmatism("header").semantics("title").signals.mkString}
      |Abstract:
      |${tp.children(ts.follows("abstract")).mkString}
      |-- Body Matter --
      |${tp.children(ts).mkString}
    """.stripMargin.trim
  }
}

class Section extends Template[String] {
  def render(de: DocElem, tp: TopologyProcessor[String], ts: TopologyState): String = {
    val rhetorical = de.semantics("rhetorical").signals.mkString
    s"${"  " * ts.depth}* (Section) ${rhetorical}\n${tp.children(ts).mkString}"
  }
}

class Paragraph extends Template[String] {
  def render(de: DocElem, tp: TopologyProcessor[String], ts: TopologyState): String = {
    val rhetorical = de.semantics("rhetorical").signals.mkString
    s"${"  " * ts.depth}* (Paragraph) ${rhetorical}\n${tp.children(ts).mkString}"
  }
}

class Sentence extends Template[String] {
  def render(de: DocElem, tp: TopologyProcessor[String], ts: TopologyState): String = {
    s"${"  " * ts.depth}* ${de.signals.mkString}\n${tp.children(ts).mkString}"
  }
}

class Generic extends Template[String] {
  def render(de: DocElem, tp: TopologyProcessor[String], ts: TopologyState): String = {
    s"${"  " * ts.depth}* ${de.sigmatics.head}\n${tp.children(ts).mkString}"
  }
}
