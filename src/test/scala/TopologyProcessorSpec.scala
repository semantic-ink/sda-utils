/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import org.scalatest._
import ink.semantic.models.SDA
import ink.semantic.models.DocElem
import ink.semantic.models.TopologyState

class TopologyProcessorSpec extends FlatSpec with Matchers {

  val corpus = SdaUtils.getSdaCorpus

  corpus.emit( SDA("example:1", "header", "title", "en", """Document Element Theory""") )
  corpus.emit( SDA("example:1", "header", "publicationDate", "ISO 8601", """2017-03-01""") )
  corpus.emit( SDA("example:1", "header", "authors", "csv", """Sven Hodapp, Marc Jacobs""") )
  corpus.emit( SDA("example:1", "header", "edition", "topology:edition", """{"edition": "a", "timestamp": 1493314352858}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "a", "rank": 10, "child": {"si": "sec1", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "a", "rank": 20, "child": {"si": "sec2", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "a", "rank": 30, "child": {"si": "sec3", "pr": "section", "edition": "b"}}""") )

  corpus.emit( SDA("example:1", "header", "edition", "topology:edition", """{"edition": "lowestRankTest", "timestamp": 1493314352858}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "lowestRankTest", "rank": 1337, "child": {"si": "sec1", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "lowestRankTest", "rank": 2018, "child": {"si": "sec2", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "lowestRankTest", "rank": 4711, "child": {"si": "sec3", "pr": "section", "edition": "b"}}""") )

  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "x", "rank": 10, "child": {"si": "sec3", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "x", "rank": 13, "child": {"si": "par1", "pr": "paragraph", "edition": "b"}}""") )
  corpus.emit( SDA("example:1", "header", "child", "topology:item", """{"edition": "x", "rank": 20, "child": {"si": "sec2", "pr": "section", "edition": "b"}}""") )

  corpus.emit( SDA("example:1", "header", "abstract", "topology:item", """{"edition": "a", "rank": 10, "child": {"si": "par3", "pr": "paragraph", "edition": "b"}}""") )


  corpus.emit( SDA("sec1", "section", "title", "en", """Introduction""") )
  corpus.emit( SDA("sec1", "section", "numbering", "ISO 2145", """1""") )
  corpus.emit( SDA("sec1", "section", "rhetorical", "rdf:about", """http://purl.org/spar/deo/Introduction""") )
  corpus.emit( SDA("sec1", "section", "edition", "topology:edition", """{"edition": "b", "timestamp": 1493314352858}""") )
  corpus.emit( SDA("sec1", "section", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "par1", "pr": "paragraph"}}""") )
  corpus.emit( SDA("sec1", "section", "child", "topology:item", """{"edition": "b", "rank": 20, "child": {"si": "par2", "pr": "paragraph"}}""") )

  corpus.emit( SDA("sec2", "section", "title", "en", """Document Decomposition""") )
  corpus.emit( SDA("sec2", "section", "numbering", "ISO 2145", """2""") )
  corpus.emit( SDA("sec2", "section", "rhetorical", "rdf:about", """http://purl.org/spar/deo/Methods""") )
  corpus.emit( SDA("sec2", "section", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "par3", "pr": "paragraph"}}""") )
  corpus.emit( SDA("sec2", "section", "child", "topology:item", """{"edition": "b", "rank": 20, "child": {"si": "fig1", "pr": "figure"}}""") )

  corpus.emit( SDA("sec3", "section", "title", "en", """Glossary""") )
  corpus.emit( SDA("sec3", "section", "numbering", "ISO 2145", """3""") )
  corpus.emit( SDA("sec3", "section", "rhetorical", "text:plain", """GLOSSARY""") )
  corpus.emit( SDA("sec3", "section", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "g1", "pr": "glossar-item"}}""") )
  corpus.emit( SDA("sec3", "section", "child", "topology:item", """{"edition": "b", "rank": 20, "child": {"si": "g2", "pr": "glossar-item"}}""") )

  corpus.emit( SDA("par1", "paragraph", "rhetorical", "en", """http://purl.org/spar/deo/Prologue""") )
  corpus.emit( SDA("par1", "paragraph", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "s1", "pr": "sentence"}}""") )
  corpus.emit( SDA("par1", "paragraph", "child", "topology:item", """{"edition": "b", "rank": 20, "child": {"si": "s2", "pr": "sentence"}}""") )
  corpus.emit( SDA("par1", "vocab:y", "concept:x", "nne", """{"dsl": "for nne"}""") )

  corpus.emit( SDA("par2", "paragraph", "rhetorical", "en", """http://purl.org/spar/deo/Motivation""") )
  corpus.emit( SDA("par2", "paragraph", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "s3", "pr": "sentence"}}""") )

  corpus.emit( SDA("par3", "paragraph", "rhetorical", "en", """http://purl.org/spar/deo/Methods""") )
  corpus.emit( SDA("par3", "paragraph", "child", "topology:item", """{"edition": "b", "rank": 10, "child": {"si": "s4", "pr": "sentence"}}""") )

  corpus.emit( SDA("fig1", "figure", "caption", "en", """Information Tetrahedron with it's semiotic dimensions.""") )
  // TODO: handling of external files / hashes to files... fp="72a5cd2da28184a7a939c344456f197ae11bb32f"
  corpus.emit( SDA("fig1", "figure", "diagram", "png", """""") )
  corpus.emit( SDA("fig1", "provenance", "author", "en", """Sven Hodapp""") )

  corpus.emit( SDA("s1", "sentence", "original", "en", """We have a long standing history in document retrieval in the biomedical sciences.""") )
  corpus.emit( SDA("s2", "sentence", "original", "en", """Several approaches have been published in the last 10 years serving the wish of the researcher to find interesting articles in special area or context.""") )

  corpus.emit( SDA("s3", "sentence", "original", "en", """Currently we see a trend in requesting of retrieval of single hypotheses and statements from the literature.""") )

  corpus.emit( SDA("s4", "sentence", "original", "en", """The novel idea is to split the documents in the atomic parts to gain more flexibility in handling the semantics, keeping track of the provenance and implementing efficient storage or retrieval.""") )


  corpus.emit( SDA("g1", "glossar-item", "term", "en", """Document Element""") )
  corpus.emit( SDA("g1", "glossar-item", "synonym", "en", """Document Component""") )
  corpus.emit( SDA("g1", "glossar-item", "description", "en", """A well defined elementary component of a document.""") )

  corpus.emit( SDA("g2", "glossar-item", "abbreviation", "en", """SDA""") )
  corpus.emit( SDA("g2", "glossar-item", "term", "en", """Semantic Digital Asset""") )
  corpus.emit( SDA("g2", "glossar-item", "description", "en", """Is a digital asset with additional semantic properties like sigmatics, pragmatics (layer, context), semantics and syntax. These properties linking to a controlled vocabular (like RDF/Ontologies).""") )

  // ---- example 2 ------

  corpus.emit( SDA("example:2", "table", "caption", "en", "Regression test for following other topology semantics") )
  corpus.emit( SDA("example:2", "table", "row", "topology:item", """{"edition": "a", "rank": 10, "child": {"si": "sec1", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:2", "table", "row", "topology:item", """{"edition": "a", "rank": 20, "child": {"si": "sec2", "pr": "section", "edition": "b"}}""") )
  corpus.emit( SDA("example:2", "table", "row", "topology:item", """{"edition": "a", "rank": 30, "child": {"si": "sec3", "pr": "section", "edition": "b"}}""") )

  "A TopologyProcessor" should "be able to list all available edtions" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    tp.listEditions("example:1", "header") should contain allOf ("a", "x")
  }

  it should "provide you a default edition (means latest edition)" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    tp.defaultEdition("example:1", "header") should equal (Option("x"))
    tp.defaultEdition("sec1", "section") should equal (Option("b"))
  }

  it should "render a hierarchy of document elements" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val expectation = """
      |* example:1
      |  * sec1
      |    * par1
      |      * s1
      |      * s2
      |    * par2
      |      * s3
      |  * sec2
      |    * par3
      |      * s4
      |    * fig1
      |  * sec3
      |    * g1
      |    * g2
    """.stripMargin.trim
    val ts = TopologyState("example:1", "header", Option("a"))
    tp.getRepresentation(ts).trim should be (expectation)
  }

  it should "retrieve document element templates from class path based on pragmatism" in {
    // TODO how to test/proof this?
    // val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.cli")
    // val ts = TopologyState("example:1", "header", Option("a"))
    // println(tp.getRepresentation(ts))
    pending
  }

  it should "be able to render only a part of the topology" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val expectation = """
      |* sec2
      |  * par3
      |    * s4
      |  * fig1
    """.stripMargin.trim
    val ts = TopologyState("sec2", "section", Option("b"))
    tp.getRepresentation(ts).trim should be (expectation)
  }

  it should "be able to follow the specified path of the edition" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val expectation = """
      |* example:1
      |  * sec3
      |    * g1
      |    * g2
      |  * par1
      |    * s1
      |    * s2
      |  * sec2
      |    * par3
      |      * s4
      |    * fig1
    """.stripMargin.trim
    val ts = TopologyState("example:1", "header", Option("x"))
    tp.getRepresentation(ts).trim should be (expectation)
  }

  it should "be possible to stop traversal at a certain depth" in {
    val ts = TopologyState("example:1", "header", Option("a"))
    val tp_1 = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple", Option(1))
    val expectation_depth1 = """
      |* example:1
      |  * sec1
      |  * sec2
      |  * sec3
    """.stripMargin.trim
    tp_1.getRepresentation(ts).trim should be (expectation_depth1)
    val tp_2 = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple", Option(2))
    val expectation_depth2 = """
      |* example:1
      |  * sec1
      |    * par1
      |    * par2
      |  * sec2
      |    * par3
      |    * fig1
      |  * sec3
      |    * g1
      |    * g2
    """.stripMargin.trim
    tp_2.getRepresentation(ts).trim should be (expectation_depth2)
  }

  it should "be able to follow another semantics than 'child'" in {
    val ts = TopologyState("example:2", "table", Option("a"))
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val expectation = """
      |* example:2
      |  * sec1
      |  * sec2
      |  * sec3
    """.stripMargin.trim
    tp.getRepresentation(ts.follows("row")).trim should be (expectation)
  }

  it should "be possible to get direct children as DocElems" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val ts = TopologyState("example:1", "header", Option("a"))
    tp.directChildrenDocElems(ts) should have length (3)
    tp.directChildrenDocElems(ts).head shouldBe a [DocElem]
    tp.directChildrenDocElems(ts).map(_.sigmatics.head) should contain theSameElementsAs Vector("sec1", "sec2", "sec3")
  }

  it should "be possible to get all children as DocElems" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val ts = TopologyState("sec2", "section", Option("b"))
    tp.allChildrenDocElems(ts) should have length (3)
    tp.allChildrenDocElems(ts).head shouldBe a [DocElem]
    tp.allChildrenDocElems(ts).map(_.sigmatics.head) should contain theSameElementsAs Vector("par3", "fig1", "s4")
  }

  it should "be able to determine the lowest rank of a hierarcy" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val ts = TopologyState("example:1", "header", Option("lowestRankTest"))
    tp.lowestRank(ts) should be (1337)
  }

  it should "be able to determine the highest rank of a hierarcy" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    val ts = TopologyState("example:1", "header", Option("lowestRankTest"))
    tp.highestRank(ts) should be (4711)
  }

  it should "be able to determine the rank of the next sibling" in {
    val tp = new TopologyProcessor[String](corpus, "ink.semantic.docelems.simple")
    // Note: The superior TopologyState will be autogenerated by the child-recursion in TopologyProcessor!
    val superiorTs = TopologyState("example:1", "header", Option("lowestRankTest"))

    val firstSibling = TopologyState("sec1", "section", Option("lowestRankTest"), 1337, 1, "child", Option(superiorTs))
    tp.upperSiblingRank(firstSibling) should be (2018)
    val inbetweenSibling = TopologyState("sec2", "section", Option("lowestRankTest"), 2018, 1, "child", Option(superiorTs))
    tp.upperSiblingRank(inbetweenSibling) should be (4711)
    val lastSibling = TopologyState("sec3", "section", Option("lowestRankTest"), 4711, 1, "child", Option(superiorTs))
    tp.upperSiblingRank(lastSibling) should be (0)

    val theresNoLowerSibling = TopologyState("sec2", "section", Option("lowestRankTest"), 0, 1, "child", Option(superiorTs))
    tp.upperSiblingRank(theresNoLowerSibling) should be (0)
    val theresNoUpperSibling = TopologyState("sec2", "section", Option("lowestRankTest"), 10000, 1, "child", Option(superiorTs))
    tp.upperSiblingRank(theresNoUpperSibling) should be (0)
  }

  "The TopologyState" should "be have modifyable se and pr properties" in {
    val ts = TopologyState("par1", "paragraph", Option("e1"))
    val ts_ = ts.pr("sentence")
    val ts__ = ts.pr("header").se("abstract")
    ts.pragmatism should be ("paragraph")
    ts.semantics should be ("child")
    ts_.pragmatism should be ("sentence")
    ts_.semantics should be ("child")
    ts__.pragmatism should be ("header")
    ts__.semantics should be ("abstract")
  }

}
