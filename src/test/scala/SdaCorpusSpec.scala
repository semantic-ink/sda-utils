/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import org.scalatest._
import ink.semantic.models.SDA
import ink.semantic.models.DocElem
import ink.semantic.models.SdaCorpus
import ink.semantic.containers.InMemoryContainer
import ink.semantic.containers.S3BucketContainer

class SdaCorpusSpec extends FlatSpec with Matchers {

  val s1 = SDA("s1", "sentence", "original", "en", "Hello World.")
  val s2 = SDA("s2", "sentence", "original", "en", "Lorem Ipsum.")

  "A SdaCorpus" should "emit all SDAs in corpus" in {

    val sc = SdaUtils.getSdaCorpus
    sc.emit(s1)
    sc.emit(s2)

    sc.ls should contain allOf (s1, s2)

  }

  it should "revoke SDAs from a corpus" in {

    val sc = SdaUtils.getSdaCorpus
    sc.emit(s1)
    sc.emit(s2)
    val revoked = SDA("s2", "sentence", "original", "en", "SHOULD NOT BE RELEVANT")
    sc.revoke(revoked)

    // sc.ls.map(_.eid) should contain allOf (s1.eid, s2.eid, revoked.eid)
    // '-> Note: only valid when event sourcing is enabled!
    sc.get.map(_.eid) should contain only (s1.eid)

  }

  it should "provide a simple query API" in {

    val sc = SdaUtils.getSdaCorpus
    sc.emit(s1)
    sc.emit(s2)
    sc.emit("p1", "paragraph", "rhetorical", "string", "introduction")
    sc.emit("p2", "paragraph", "rhetorical", "string", "discussion")
    sc.emit("s3", "sentence", "original", "en", "Green tea is finest tea.")
    sc.emit("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03304639", "text-offset", "(0,8)")
    sc.emit("s4", "sentence", "original", "de", "Guten Tag!")

    sc.sigmatics("s3").get should have length (2)
    sc.pragmatism("paragraph").get should have length (2)
    sc.semantics("http://purl.obolibrary.org/obo/FOODON_03304639").get should have length (1)
    sc.syntax("en").get should have length (3)
    sc.isEmpty should be (false)
    sc.length should be (7)

    var chained = sc.pragmatism("sentence")
    chained.get should have length (4)
    chained = chained.syntax("de")
    chained.get should have length (1)
    chained.length should be (1)
    chained = chained.semantics("not_findable")
    chained.isEmpty should be (true)

  }

  it should "provide document elements it contains" in {

    val sc = SdaUtils.getSdaCorpus
    sc.emit(s1)
    sc.emit(s2)
    sc.emit("p1", "paragraph", "rhetorical", "string", "introduction")
    sc.emit("p2", "paragraph", "rhetorical", "string", "discussion")
    sc.emit("s3", "sentence", "original", "en", "Green tea is finest tea.")
    sc.emit("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03304639", "text-offset", "(0,8)")
    sc.emit("s4", "sentence", "original", "de", "Guten Tag!")

    val de = sc.getDocElem("s3")
    de.get should have length (2)
    de.sigmatics should be (Set("s3"))

    sc.getDocElems should have length (6)
    sc.getDocElems.head shouldBe a [DocElem]

  }

  it should "guarantee that the newester version of an emited SDA has priority over the internal state (regression)" in {

    val firstVersion = SDA("s1", "sentence", "original", "en", "Test 123", "", 1L)
    val secondVersion = SDA("s1", "sentence", "original", "en", "Other Text", "", 2L)

    // there exists already one docelem
    val corpus = SdaUtils.getSdaCorpus
    corpus.emit(firstVersion)
    corpus.flush
    // retrieve this docelem, so it will be cached by corpus state
    corpus.getDocElem("s1")
    corpus.get.head.ts should be (firstVersion.ts)
    corpus.get.size should be (1)
    corpus.emit(secondVersion)
    corpus.get.size should be (1)
    corpus.get.head.ts should be (secondVersion.ts)
    corpus.flush
    corpus.get.head.ts should be (secondVersion.ts)
    // also when re-fetching the docelem
    corpus.getDocElem("s1")
    corpus.get.head.ts should be (secondVersion.ts)

  }

  it should "merge SDAs of the same document element but with multiple sigmatics" in {
    // TODO we have first to define the semantics to be used for this task
    pending
  }

}
