/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.models

import org.scalatest._

class SdaSpec extends FlatSpec with Matchers {

  val s1 = SDA("s1", "sentence", "original", "en", "Hello World.")

  "SDA.hashCode" should "implement hashCode based on semiotic properties" in {
    val otherAsset = SDA("s1", "sentence", "original", "en", "Other asset")
    otherAsset.hashCode should be (s1.hashCode)
    val otherSi = SDA("xx", "sentence", "original", "en", "Hello World.")
    otherSi.hashCode should not be (s1.hashCode)
    val otherPr = SDA("s1", "paragraph", "original", "en", "Hello World.")
    otherPr.hashCode should not be (s1.hashCode)
    val otherSe = SDA("s1", "sentence", "translation", "en", "Hello World.")
    otherSe.hashCode should not be (s1.hashCode)
    val otherSy = SDA("s1", "sentence", "original", "de", "Hello World.")
    otherSy.hashCode should not be (s1.hashCode)
  }

  it should "consider also the optional fingerprint in hashCode" in {
    val fp1 = SDA("s1", "sentence", "original", "en", "Hello World.", "fp1")
    val fp2 = SDA("s1", "sentence", "original", "en", "Hello World.", "fp2")
    fp1.hashCode should not be (fp2.hashCode)
  }

  it should "consider also the optional fingerprint in hashCode also without asset" in {
    val fp1 = SDA("s1", "sentence", "original", "en", "", "fp1")
    val fp2 = SDA("s1", "sentence", "original", "en", "", "fp2")
    fp1.hashCode should not be (fp2.hashCode)
  }

  it should "not consider editstamp in hashCode" in {
    val edit1 = SDA("s1", "sentence", "original", "en", "Hello World.", "", 4L)
    val edit2 = SDA("s1", "sentence", "original", "en", "Hello World!", "", 8L)
    edit1.hashCode should be (edit2.hashCode)
  }

  val s1a = SDA("s1", "sentence", "original", "en", "Hello World?", "", 1L)
  val s1b = SDA("s1", "sentence", "original", "en", "Hello World.", "", 2L)
  val s1c = SDA("s1", "sentence", "original", "en", "Hello World!", "", 3L)
  val s2a = SDA("s2", "sentence", "original", "en", "Lorem Ipsum.", "", 4L)
  val s3a = SDA("s3", "sentence", "original", "en", "Green tea is finest tea.", "", 5L)
  val s3b = SDA("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03304639", "text-offset", "(0,8)", "08", 6L)
  val s3c = SDA("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03315081", "text-offset", "(6,8)", "68", 7L)
  val s3d = SDA("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03315081", "text-offset", "(20,22)", "2022", 8L)
  val s3e = SDA("s3", "FOODON", "http://purl.obolibrary.org/obo/FOODON_03315081", "text-offset", "(20,22,also_known_as=tea)", "2022", 9L)

  "SDA.equals and Set[SDA]" should "collapse multiple editions of the same SDA into (the newest) one" in {
    // Use Case: Same semiotic properties, but different asset, if put into a set they should collapse in such a way that the newest editstamp "wins".
    val seq1 = List(s1a, s1b, s1c).sorted.reverse
    seq1.toSet[SDA] should have size 1
    seq1.toSet[SDA].head.eid should be (s1c.eid)
    // TODO Maybe we need to add a helper function in SdaUtils or SdaCorpus (sort and so)
    val seq2 = List(s1c, s1b, s1a)
    seq2.toSet[SDA] should have size 1
    seq2.toSet[SDA].head.eid should be (s1c.eid)
  }

  // Fingerprint und Editstamp sind buchhalterische Eigenschaften.
  // Der Editstamp führt eine Zeitdimension hinzu, sodass die neuste Version identifiziert werden kann.
  // Der Fingerprint sorgt für vektorisierte SDAs, damit mehere SDAs in der gleichen Semantischen/Semiotischen Dimension existieren können. (daher optional -> muss vielleicht in der jeweiligen spec zu einer syntax definiert sein, ob das sein darf)

  it should "preserve multiple SDAs with the same semiotic properties but distinct fingerprints" in {
    // Use Case: This is relevant e.g. for NER tag annotations, because they may have the same semiotic properties, but different content (other position in text), so they have to be distinct SDAs. (This allows a set of NER tags with the same properties on different text offsets)
    // NOTE: The fingerprint could be constructed from the asset, for example from the text offset part of a NER tag DSL. Thus it must not be necessarily a hash or uuid.
    // NOTE: The fingerprint is optional, default is empty! It should only be used on "annotations" fulfilling the above condition.
    s3d should equal (s3e)
  }

  it should "collapse only duplicates based on the fingerprint" in {
    // Use Case: maybe semantically the same NER tag is written on the same text offset position, but with a updated NER tag DSL (e.g. a synonymous/new id was added for it)
    val seq = List(s3c, s3d, s3e).sorted.reverse
    seq.toSet[SDA].map(_.eid) should contain only (s3c.eid, s3e.eid)
  }

  it should "produce the valid current state of a corpus when put into a Set" in {
    val seq = List(s1a, s1b, s1c, s2a, s3a, s3b, s3c, s3d, s3e).sorted.reverse
    seq.toSet[SDA].map(_.eid) should contain only (s1c.eid, s2a.eid, s3a.eid, s3b.eid, s3c.eid, s3e.eid)
  }

  it should "be possible to remove a SDA from a Set only by its semiotic features plus fingerprint" in {

    val remove = SDA("s1", "sentence", "original", "en", "not relevant", "", 100L)
    val seq = List(s1a, s2a)
    val set = seq.toSet[SDA] - remove
    set.toSeq should  contain only (s2a)

  }

  "SDA.compare" should "be sorted by sigmatics and editstamp" in {
    val unsorted = List(s3a, s1a, s1b, s3c, s3e, s3b, s2a, s3d, s1c)
    val sorted = unsorted.sorted
    // NOTE: We can not trust the hashCode ;)
    sorted(0).si shouldBe (s1a.si)
    sorted(0).eid shouldBe (s1a.eid)
    sorted(3).si shouldBe (s2a.si)
    sorted(3).eid shouldBe (s2a.eid)
    sorted(8).si shouldBe (s3e.si)
    sorted(8).eid shouldBe (s3e.eid)
  }

  "SDA.hasExternalAsset" should "tell if the actual (maybe large) asset is found as temporary file" in {

    val external = SDA("f1", "figure", "diagram", "png", "", "d8c22039a5f2879f9c97bb1cb0e3de76")
    external.hasExternalAsset should be (true)

    val others = List(s1a, s1b, s1c, s2a, s3a, s3b, s3c, s3d, s3e)
    others.map(_.hasExternalAsset should be (false))

  }

}
