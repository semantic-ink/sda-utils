/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import java.time.Instant

import org.scalatest._

import ink.semantic.SdaUtils
import ink.semantic.models._

class ModelPragmatismSpec extends FlatSpec with Matchers {

  "A Provenance pragmatism" should "generate SDAs with provenance layer" in {

    val pivot = SDA("p1", "paragraph", "sentences", "en", "Hello World.")
    val annotatorName = "some-annotator"
    val annotatorVersion = "1.0.0"
    val time = Instant.now

    val prov = pragmatisms.Provenance(pivot, annotatorName, annotatorVersion, time)
    val sdas = prov.mkSdas

    sdas.filter(_.se.startsWith("entity-si_")).head.asset should be ("p1")
    sdas.filter(_.se.startsWith("time_")).head.asset should be (time.toString)
    sdas.filter(_.se.startsWith("annotator-name_")).head.asset should be (annotatorName)
    sdas.filter(_.se.startsWith("annotator-version_")).head.asset should be (annotatorVersion)

  }

  it should "deserialize from JSON to Annotation object" in {
    ;
  }

}
