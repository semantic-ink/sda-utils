/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic.formats

import org.scalatest._

import java.nio.charset.StandardCharsets

import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils

import ink.semantic.models.SDA
import ink.semantic.models.SdaEvent
import ink.semantic.models.DocElem
import ink.semantic.models.SdaCorpus
import ink.semantic.containers.InMemoryContainer
import ink.semantic.containers.S3BucketContainer
import ink.semantic.models.dsls.TopologyItem


class SdaOutFormatSpec extends FlatSpec with Matchers {

  "The SdaOutFormat" should "read the stdout format from stream" in {

    val stdout =
      """p1\u0009paragraph\u0009sentences\u0009de\u0009 \u0009Hallo Welt.␊Das ist doch was!
        |p1\u0009paragraph\u0009sentences\u0009de\u0009x\u0009Hallo Welt.␊Das ist doch was!
        |p2\u0009paragraph\u0009sentences\u0009en\u0009 \u0009Some one liner.
        |f1\u0009asset\u0009empty\u0009txt\u0009 \u0009
        |""".stripMargin
    val stream = IOUtils.toInputStream(stdout)

    val events = SdaOutFormat.read(stream)
    val elist = events.toList

    elist(0).event should be (' ')
    elist(0).sda.si should be ("p1")
    elist(0).sda.pr should be ("paragraph")
    elist(0).sda.se should be ("sentences")
    elist(0).sda.sy should be ("de")
    elist(0).sda.asset should be ("Hallo Welt.\nDas ist doch was!")
    elist(1).event should be ('x')
    elist(2).sda.asset should be ("Some one liner.")
    // regression for the case when the asset is empty
    elist(3).sda.si should be ("f1")
    elist(3).sda.pr should be ("asset")
    elist(3).sda.se should be ("empty")
    elist(3).sda.sy should be ("txt")
    elist(3).sda.asset should be ("")

  }

  it should "generate the stdout format" in {

    val s1 = SdaEvent(' ', SDA("p1", "paragraph", "sentences", "de", "Hallo Welt.\nDas ist doch was!"))
    val s2 = SdaEvent('x', SDA("p2", "paragraph", "sentences", "de", "Das ist ein Test."))
    val s3 = SdaEvent(' ', SDA("contains\nnewline", "contains\ttab", "contains\rcarriagereturn", "en.txt", "Should be transformed to it's symbols"))

    SdaOutFormat.mkFormat(s1) should be(
      "p1\tparagraph\tsentences\tde\t \tHallo Welt.␊Das ist doch was!\n")

    SdaOutFormat.mkFormat(s2) should be(
      "p2\tparagraph\tsentences\tde\tx\tDas ist ein Test.\n")

      SdaOutFormat.mkFormat(s3) should be(
        "contains␊newline\tcontains␉tab\tcontains␍carriagereturn\ten.txt\t \tShould be transformed to it's symbols\n")
  }

  it should "write the stdout format to file" in {

    val s1 = SdaEvent(' ', SDA("p1", "paragraph", "sentences", "de", "Hallo Welt.\nDas ist doch was!"))
    val s2 = SdaEvent('x', SDA("p2", "paragraph", "sentences", "de", "Das ist ein Test."))
    val s3 = SdaEvent(' ', SDA("contains\nnewline", "contains\ttab", "contains\rcarriagereturn", "en.txt", "Should be transformed to it's symbols"))

    val file = SdaOutFormat.write(Seq(s1, s2, s3).toIterator)
    file.exists should be (true)
    file.getName should endWith ("sdaout")
    file.length should be (213)

    val content = FileUtils.readFileToString(file, StandardCharsets.UTF_8)
    content should include ("p1")
    content should include ("Hallo Welt.")
    content should include ("Should be transformed")

  }

  it should "read and write the stdout format from compressed file" in {

    val s1 = SdaEvent(' ', SDA("p1", "paragraph", "sentences", "de", "Hallo Welt.\nDas ist doch was!"))
    val s2 = SdaEvent('x', SDA("p2", "paragraph", "sentences", "de", "Das ist ein Test."))
    val s3 = SdaEvent(' ', SDA("contains\nnewline", "contains\ttab", "contains\rcarriagereturn", "en.txt", "Should be transformed to it's symbols"))

    val file = SdaOutFormat.writeCompressed(Seq(s1, s2, s3).toIterator)
    file.exists should be (true)
    file.getName should endWith ("sdaout.gz")
    file.length.toDouble should be (169.0 +- 5.0)

    val stream = FileUtils.openInputStream(file)
    val events = SdaOutFormat.readCompressed(stream)
    val elist = events.toList

    elist.length should be (3)
    elist(0).sda.si should be ("p1")
    elist(1).sda.si should be ("p2")
    elist(2).sda.si should be ("contains\nnewline")

  }

  it should "be performant" in {

    // generate some sdas
    val sdaSource: Stream[SdaEvent] = {
      def loop(sda: SdaEvent, i: Int): Stream[SdaEvent] = {
        val count = i + 1
        val event = SdaEvent(' ', SDA("p1", "paragraph", "sentences", "en", s"Make a performance test\nEntity # ${count}!"))
        sda #:: loop(event, count)
      }
      loop(SdaEvent(' ', SDA("p1", "paragraph", "sentences", "en", s"First!")), 0)
    }
    // and store them
    val eventCount = 100000
    val file = SdaOutFormat.write(sdaSource.take(eventCount).iterator)
    file.exists should be (true)

    val t0 = System.nanoTime()
    val stream = FileUtils.openInputStream(file)
    val events = SdaOutFormat.read(stream)
    events.foreach{ item => item }
    val t1 = System.nanoTime()

    file.delete

    println(s"[performance report] parsed ${eventCount} events in ${(t1 - t0) / 1000000.0} ms" )

  }

}
