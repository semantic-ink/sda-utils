/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import org.scalatest._

import ink.semantic.models.SDA
import ink.semantic.models.dsls.Annotation

class ModelsDslSpec extends FlatSpec with Matchers {

  val comment = SDA("c1", "comment", "text", "en", "Not bad!")
  val position = SDA("s1", "sentence", "original", "en", "apple is the most valuable company in the world")
  val coordinates = List(13, 22)

  "A Annotation DSL" should "serialize to JSON string" in {

    Annotation(comment, position, coordinates).asJson should be ("""{"comment":{"si":"c1","pr":"comment","se":"text","sy":"en","fp":""},"position":{"si":"s1","pr":"sentence","se":"original","sy":"en","fp":"","coordinates":[13,22]}}""")

  }

  it should "deserialize from JSON to Annotation object" in {

    val jsonStr = """{"comment":{"si":"c1","pr":"comment","se":"text","sy":"en","fp":""},"position":{"si":"s1","pr":"sentence","se":"original","sy":"en","fp":"","coordinates":[13,22]}}"""

    Annotation.apply(jsonStr) should be (Annotation(comment, position, coordinates))

  }

}
