/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import org.scalatest._

import org.apache.commons.io.IOUtils

import ink.semantic.models.SDA
import ink.semantic.models.SdaEvent
import ink.semantic.models.DocElem
import ink.semantic.models.SdaCorpus
import ink.semantic.containers.InMemoryContainer
import ink.semantic.containers.S3BucketContainer
import ink.semantic.models.dsls.TopologyItem


class SdaUtilsSpec extends FlatSpec with Matchers {

  val credentials = new InMemoryContainer(
    List(
      SDA("tester", "s3:credentials", "ENDPOINT", "string", "http://localhost:9000"),
      SDA("tester", "s3:credentials", "ACCESS_KEY", "string", "J0148KUQZZMATIGD1C1N"),
      SDA("tester", "s3:credentials", "SECRET_KEY", "string", "ZVRPanENsg4lLXehZpaM1bXcksItQ5MqiKBvVV4D")
    )
  )

  val invalidCredentials = new InMemoryContainer()

  def corpusId = java.util.UUID.randomUUID().toString

    // SDAs of the environment
    def setEnv = {
      System.setProperty("MINIO_ENDPOINT_SDA", "tester|s3:credentials|ENDPOINT|string|http://localhost:9000")
      System.setProperty("MINIO_ACCESS_KEY_SDA", "tester|s3:credentials|ACCESS_KEY|string|J0148KUQZZMATIGD1C1N")
      System.setProperty("MINIO_SECRET_KEY_SDA", "tester|s3:credentials|SECRET_KEY|string|ZVRPanENsg4lLXehZpaM1bXcksItQ5MqiKBvVV4D")
    }

    def unsetEnv = {
      System.clearProperty("MINIO_ENDPOINT_SDA")
      System.clearProperty("MINIO_ACCESS_KEY_SDA")
      System.clearProperty("MINIO_SECRET_KEY_SDA")
    }


  "SdaUtils:getSdaCorpus" should "get via default a in-memory corpus" in {

    val sc = SdaUtils.getSdaCorpus
    sc shouldBe a [SdaCorpus]
    sc shouldBe a [InMemoryContainer]

  }

  it should "get via default a s3 corpus when providing credentials" in {

    val s3corpus = SdaUtils.getSdaCorpus(corpusId, credentials)

    s3corpus shouldBe a [SdaCorpus]
    s3corpus shouldBe a [S3BucketContainer]

    val imcorpus = SdaUtils.getSdaCorpus(corpusId, invalidCredentials)

    imcorpus shouldBe a [SdaCorpus]
    imcorpus shouldBe a [InMemoryContainer]

  }

  it should "get via default a s3 corpus when credentials are found in env" in {

    unsetEnv
    val corpusWithoutEnv = SdaUtils.getSdaCorpus(corpusId)
    corpusWithoutEnv shouldBe a [SdaCorpus]
    corpusWithoutEnv shouldBe a [InMemoryContainer]

    setEnv
    val finalCorpusId = corpusId
    val corpusWithEnv = SdaUtils.getSdaCorpus(finalCorpusId)
    corpusWithEnv shouldBe a [SdaCorpus]
    corpusWithEnv shouldBe a [S3BucketContainer]
    // regression: case when recreating corpus
    val corpusWithEnvRecreated = SdaUtils.getSdaCorpus(finalCorpusId)
    corpusWithEnvRecreated shouldBe a [SdaCorpus]
    corpusWithEnvRecreated shouldBe a [S3BucketContainer]
    unsetEnv


  }

  "SdaUtils:environment" should "convert a environment variable to a SDA" in {

    def fakeGetEnv(name: String) = "The content"
    val envSda = SdaUtils.env2sda("VAR_NAME", "posix")(fakeGetEnv)

    envSda.si should include ("@") // e.g. user@hostname
    envSda.pr should be ("posix:environment-variable")
    envSda.se should be ("VAR_NAME")
    envSda.sy should be ("string")
    envSda.as should be ("The content")

  }

  it should "parse a special constructed environment variable into a SDA" in {

    def fakeGetEnv(name: String) = "person:sh|s3:credentials|user|string|sven"
    val envSda = SdaUtils.env2sda("S3_USER_SDA", "posix")(fakeGetEnv)

    envSda.si should be ("person:sh")
    envSda.pr should be ("s3:credentials")
    envSda.se should be ("user")
    envSda.sy should be ("string")
    envSda.as should be ("sven")

  }

  it should "also parse a lower cased environment variable into a SDA" in {

    def fakeGetEnv(name: String) = "person:sh|s3:credentials|user|string|sven"
    val envSda = SdaUtils.env2sda("s3_user_sda", "posix")(fakeGetEnv)

    envSda.si should be ("person:sh")
    envSda.pr should be ("s3:credentials")
    envSda.se should be ("user")
    envSda.sy should be ("string")
    envSda.as should be ("sven")

  }

  it should "pull the system environment into SDAs" in {

    val sdas = SdaUtils.pullSystemEnv
    sdas.size should be > (1)
    sdas.filter(_.se == "PATH").size should be (1)
    sdas.filter(_.se == "PATH").head.asset should include ("/bin")

  }

  it should "pull the java property environment into SDAs" in {

    val sdas = SdaUtils.pullJavaPropertyEnv
    sdas.size should be > (1)

  }

  "SdaUtils:topology" should "produce a topological link between two document elements" in {

    val parent = SDA("p1", "paragraph", "rhetorical", "string", "discussion")
    val child = SDA("s1", "sentence", "original", "en", "Hello world.")
    val rank = 10
    val topoSda = SdaUtils.link(parent, child, rank)

    topoSda.si should be ("p1")
    topoSda.pr should be ("paragraph")
    topoSda.se should be ("child")
    topoSda.sy should be ("topology:item")
    val parsedTopo = TopologyItem.apply(topoSda.as)
    parsedTopo shouldBe a [TopologyItem]
    parsedTopo.childSi should be ("s1")
    parsedTopo.childPr should be ("sentence")
    parsedTopo.rank should be (rank)

  }

  "SdaUtils.provideFingerprint" should "enrich a SDA with a fingerprint, if FingerprintGenerator can handle it's signal" in {

    val ti = TopologyItem("test-edition", 10, "s1", "sentence", None)
    val sda = SDA("p1", "paragraph", "child", "topology:item", ti.asJson)
    sda.fp should be ("")

    val sda_ = SdaUtils.provideFingerprint(sda)
    sda_.fp should not be ("")

  }

  it should "generate a empty fingerprint for a unknown syntax with any signal" in {

    val syntax = "this/syntax/is/unknown/for/this/system"
    val signal = "Hello World"
    val sda = SDA("s1", "sentence", "original", syntax, signal)
    sda.fp should be ("")

    val sda_ = SdaUtils.provideFingerprint(sda)
    sda_.fp should be ("")

  }

  it should "throw an Exception on a malformed signal" in {

    val sda = SDA("p1", "paragraph", "child", "topology:item", "{ malformed }")
    an [Exception] should be thrownBy SdaUtils.provideFingerprint(sda)

  }

  "SdaUtils:sentenceHash" should "enrich a SDA with a simple sentence hash" in {
    val s1 = SDA("", "sentence", "original", "de", "Das ist ein Test.")
    val s2 = SDA("", "sentence", "original", "de", "ein test ist das!")
    val s3 = SDA("", "sentence", "original", "de", "das is ein test")
    val s4 = SDA("", "sentence", "original", "de", "das ist ein fest")
    val h1 = SdaUtils.addSimpleSentenceHash(s1)
    val h2 = SdaUtils.addSimpleSentenceHash(s2)
    val h3 = SdaUtils.addSimpleSentenceHash(s3)
    val h4 = SdaUtils.addSimpleSentenceHash(s4)
    h1.si should be (h2.si)
    h1.si should not be (h3.si)
    h1.si should not be (h4.si)
  }

  "SdaUtils:xml" should "serialize SDAs into our XML format" in {

    val corpusId = "somd_id"

    val onTime1 = SDA("example:1", "header", "title", "en", "Document")
    val onTime2 = SDA("example:1", "header", "title", "en", "Document Element")
    val onTime3 = SDA("example:1", "header", "title", "en", "Document Element Theory")
    val withFp1 = SDA("example:1", "header", "authors", "en", "Sven", "fp1")
    val withFp2 = SDA("example:1", "header", "authors", "en", "Sven H", "fp2")

    val xmlObj = SdaUtils.mkXml( corpusId, Seq(onTime1, onTime2, onTime3, withFp1, withFp2) )

    (xmlObj \ "sda") should be (<corpus>
      <sda si="example:1" pr="header" se="authors" sy="en" fp="fp2" ts={withFp2.ts.toString}><![CDATA[Sven H]]></sda>
      <sda si="example:1" pr="header" se="authors" sy="en" fp="fp1" ts={withFp1.ts.toString}><![CDATA[Sven]]></sda>
      <sda si="example:1" pr="header" se="title" sy="en" fp="" ts={onTime3.ts.toString}><![CDATA[Document Element Theory]]></sda>
    </corpus> \ "sda")

  }

  it should "deserialize our XML format to SDAs" in {

    val xmlObj = <corpus>
      <sda si="example:1" pr="header" se="authors" sy="en" fp="fp2" ts={3L.toString}><![CDATA[Sven H]]></sda>
      <sda si="example:1" pr="header" se="authors" sy="en" fp="fp1" ts={2L.toString}><![CDATA[Sven]]></sda>
      <sda si="example:1" pr="header" se="title" sy="en" fp="" ts={1L.toString}><![CDATA[Document Element Theory]]></sda>
    </corpus>

    val s1 = SDA("example:1", "header", "title", "en", "Document Element Theory", "", 1L)
    val s2 = SDA("example:1", "header", "authors", "en", "Sven", "fp1", 2L)
    val s3 = SDA("example:1", "header", "authors", "en", "Sven H", "fp2", 3L)

    SdaUtils.mkSdas(xmlObj) should contain allOf (s1, s2, s3)
    SdaUtils.mkSdas(xmlObj).filter(_.fp == "fp2").head.timestamp should be (3L)

  }

  it should "help to transform scala XML to a pretty XML string" in {

    SdaUtils.mkXmlString(<corpus></corpus>) should startWith ("""<?xml version="1.0" encoding="UTF-8"?>""")

  }

  it should "(regression) serialized XML should preserve newlines" in {
    // Ticket: https://github.com/scala/scala-xml/issues/75

    val xmlObj = <corpus>preserve
    the
    newlines
    </corpus>

    SdaUtils.mkXmlString(xmlObj) should include ("preserve\n")
    SdaUtils.mkXmlString(xmlObj) should include ("the\n")
    SdaUtils.mkXmlString(xmlObj) should include ("newlines\n")

  }

  it should "help to create annotations on annotations" in {

    val thing = SDA("s1", "sentence", "original", "de", "Click here to...")
    val thingCoordinates = List(6, 9)
    val comment = SDA("c1", "citation", "link", "url", "https://zenodo.org/record/11466")

    val annotationPr = "emphasis"
    val annotationSe = "link"

    val annot = SdaUtils.annotate(thing, thingCoordinates, comment, annotationPr, annotationSe)

    annot.si should be ("s1")
    annot.pr should be ("emphasis")
    annot.se should be ("link")
    annot.sy should be ("annotation")

  }

  it should "help to generate a unique id of an SDA" in {

    val sda = SDA("p1", "paragraph", "sentences", "en", "Hello World.")
    SdaUtils.sdaId(sda) should be ("3598579785")

  }

  it should "get a agent definition retrieved from env vars and as fallback the infos of this library" in {

    SdaUtils.getAnnotatorName should be ("sda-utils")
    SdaUtils.getAnnotatorVersion should fullyMatch regex """.*\..*\..*"""

  }


}
