/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ink.semantic

import java.net.URI

import scala.xml.XML

import org.scalatest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.Await
import scala.util.{Success, Failure}

import ink.semantic.models.SDA
import ink.semantic.models.DocElem
import ink.semantic.models.SdaCorpus
import ink.semantic.models.dsls.UriAnnotation
import ink.semantic.containers.InMemoryContainer
import ink.semantic.containers.S3BucketContainer
import ink.semantic.formats.SdaOutFormat

class S3BucketContainerSpec extends FlatSpec with Matchers {

  val credentials = new InMemoryContainer(
    List(
      SDA("tester", "s3:credentials", "ENDPOINT", "string", "http://localhost:9000"),
      SDA("tester", "s3:credentials", "ACCESS_KEY", "string", "J0148KUQZZMATIGD1C1N"),
      SDA("tester", "s3:credentials", "SECRET_KEY", "string", "ZVRPanENsg4lLXehZpaM1bXcksItQ5MqiKBvVV4D")
    )
  )

  val invalidCredentials = new InMemoryContainer()

  val corpusId = java.util.UUID.randomUUID().toString
  val client = S3BucketContainer.extractCredentials(credentials)
  S3BucketContainer.makeBucket(corpusId, credentials)

  val filename = "d8c22039a5f2879f9c97bb1cb0e3de76.png"
  val fstream = this.getClass.getResourceAsStream(s"/${filename}")
  client.putObject(corpusId, filename, fstream, fstream.available(), "application/octet-stream")

  val internalSda = SDA("s2", "sentence", "original", "en", "Lorem Ipsum.")
  val externalSda = SDA("fig1", "figure", "diagram", "png", "")

  val uri = new URI(s"s3://${corpusId}/${filename}")
  val uriAnnot = UriAnnotation(externalSda, uri)
  val externalSdaCompanion = SDA("fig1", "external-asset", "location", uriAnnot.syntax, uriAnnot.asJson)

  val invalidUri = new URI(s"s3://${corpusId}/not-existent")
  val invalidUriAnnot = UriAnnotation(externalSda, invalidUri)
  val invalidExternalSdaCompanion = SDA("fig1", "external-asset", "location", invalidUriAnnot.syntax, invalidUriAnnot.asJson)

  "A S3BucketContainer" should "expose (and cache) internal and external assets as file" in {

    val corpus = new S3BucketContainer(corpusId, credentials)

    // Note: getAssetAsFile virtually static and uses the capabilities of the corpus to retrieve external assets.

    val internalAssetFile = corpus.getAssetAsFile(internalSda)
    internalAssetFile shouldBe a [java.io.File]
    internalAssetFile.exists should be (true)
    internalAssetFile.getName should startWith (internalSda.se)
    internalAssetFile.length should be (internalSda.asset.size)

    // for the external asset we have to provide a annotation with the physical location of the asset
    corpus.emit(externalSdaCompanion)

    val externalAssetFile = corpus.getAssetAsFile(externalSda)
    externalAssetFile shouldBe a [java.io.File]
    externalAssetFile.exists should be (true)
    externalAssetFile.getName should endWith (externalSda.sy)
    externalAssetFile.length should be (155501)

  }

  it should "expose external asset which can not be found as empty file" in {

    val corpus = new S3BucketContainer(corpusId, credentials)

    // for the external asset we have to provide a annotation with the physical location of the asset
    // but in this case the location is not valid (e.g. anymore)
    corpus.emit(invalidExternalSdaCompanion)

    val externalAssetFile = corpus.getAssetAsFile(externalSda)
    externalAssetFile shouldBe a [java.io.File]
    externalAssetFile.exists should be (true)
    externalAssetFile.getName should endWith (externalSda.sy)
    externalAssetFile.length should be (0)

  }

  it should "generate a skos layer from an object" in {

    val corpus = new S3BucketContainer(corpusId, credentials)
    val de = corpus.getDocElem(filename)

    de.semantics("altLabel").get.head.asset should be ("d8c22039a5f2879f9c97bb1cb0e3de76")
    de.semantics("content").get.head.asset should be ("")
    de.pragmatism("external-asset").get.head.se should be ("location")

  }

  it should "generate a list layer from a prefix (containg multiple objects)" in {

    val corpus = new S3BucketContainer(corpusId, credentials)
    val de = corpus.getDocElem("d8c")

    val result = de.semantics("item_0").get.sorted

    result(0).asset should be (filename)
    result(1).syntax should be ("docelem-citation.json")

  }

  it should "flush a coherent session to s3" in {

    val corpus = new S3BucketContainer(corpusId, credentials)

    corpus.emit("x", "pr", "se", "sy", "asset")
    corpus.emit("y", "pr", "se", "sy", "asset")

    val sessionName = corpus.flushSession
    val obj = client.getObject(corpusId, s"$sessionName.sdaout")
    obj shouldBe a [java.io.InputStream]
    val sdas = SdaOutFormat.read(obj).toList
    sdas.size should be (2)

  }

  it should "retrive a session and integrate those SDAs into in-memory" in {

    var corpus = new S3BucketContainer(corpusId, credentials)
    corpus.emit("xx", "pr", "se", "sy", "asset")
    corpus.emit("yy", "pr", "se", "sy", "asset")
    val sessionName = corpus.flushSession(java.util.UUID.randomUUID().toString)

    corpus = new S3BucketContainer(corpusId, credentials)
    corpus.emit("xx", "pr", "se", "sy", "this should win")
    val sessionName2 = corpus.flushSession(java.util.UUID.randomUUID().toString)

    corpus = new S3BucketContainer(corpusId, credentials)
    corpus.getSession(sessionName)
    corpus.getSession(sessionName2)
    val stateSDAs = corpus.get

    stateSDAs.size should be (2)
    stateSDAs.filter(_.asset == "this should win").size should be (1)

  }

  it should "be possible to list sessions matching a certain prefix" in {

    val corpus = new S3BucketContainer(corpusId, credentials)
    val sessionChunkName_aa = corpus.flushSession("session/aa")
    val sessionChunkName_ab = corpus.flushSession("session/ab")

    corpus.lsSessions("session/a") should be (List(sessionChunkName_aa, sessionChunkName_ab))

  }

  it should "be possible to flush multiple times to an editable session" in {

    val sessionId = java.util.UUID.randomUUID().toString

    var corpus = new S3BucketContainer(corpusId, credentials)
    corpus.useEditableSession(sessionId)
    corpus.emit("x", "pr", "se", "sy", "asset")
    corpus.emit("y", "pr", "se", "sy", "asset")
    corpus.flushSession(sessionId)

    corpus = new S3BucketContainer(corpusId, credentials)
    corpus.useEditableSession(sessionId)
    corpus.get.size should be (2)

    // edit the same session again
    corpus = new S3BucketContainer(corpusId, credentials)
    corpus.useEditableSession(sessionId)
    corpus.emit("z", "pr", "se", "sy", "asset")
    corpus.flushSession(sessionId)

    corpus = new S3BucketContainer(corpusId, credentials)
    corpus.useEditableSession(sessionId)
    corpus.get.size should be (3)

  }

  it should "provide the compactation (consolidated session) of session chunks" in {

      val sessionId = java.util.UUID.randomUUID().toString

      var corpus_0 = new S3BucketContainer(corpusId, credentials)
      corpus_0.useEditableSession(sessionId)
      corpus_0.emit(SDA("t0", "list", "item_0", "txt", "time 0"))
      val chunk_0 = corpus_0.flushSession(sessionId)

      var corpus_1 = new S3BucketContainer(corpusId, credentials)
      corpus_1.useEditableSession(sessionId)
      corpus_1.emit(SDA("t1", "list", "item_0", "txt", "time 1"))
      val chunk_1 = corpus_1.flushSession(sessionId)

      var corpus_2 = new S3BucketContainer(corpusId, credentials)
      corpus_2.useEditableSession(sessionId)
      corpus_2.emit(SDA("t2", "list", "item_0", "txt", "time 2"))
      val chunk_2 = corpus_2.flushSession(sessionId)

      for(sessionChunkName <- Seq(chunk_0, chunk_1, chunk_2)) {
        val obj = client.getObject(corpusId, s"$sessionChunkName.sdaout")
        obj shouldBe a [java.io.InputStream]
      }

      val corpus = new S3BucketContainer(corpusId, credentials)
      corpus.useEditableSession(sessionId)
      corpus.compactation

      val obj = client.getObject(corpusId, s"$sessionId/session.sdaout")
      obj shouldBe a [java.io.InputStream]
      val events = SdaOutFormat.read(obj).toList
      events.size should be (3)

  }

  it should "be possible to call compactation multiple times without information loss" in {

    val sessionId = java.util.UUID.randomUUID().toString

    var corpus = new S3BucketContainer(corpusId, credentials)
    corpus.useEditableSession(sessionId)

    corpus.emit(SDA("t0", "list", "item_0", "txt", "time 0"))
    corpus.flushSession(sessionId)
    corpus.emit(SDA("t1", "list", "item_0", "txt", "time 0"))
    corpus.flushSession(sessionId)

    corpus.compactation

    val obj = client.getObject(corpusId, s"$sessionId/session.sdaout")
    obj shouldBe a [java.io.InputStream]
    val events = SdaOutFormat.read(obj).toList
    events.size should be (2)

    corpus.compactation

    val obj_next = client.getObject(corpusId, s"$sessionId/session.sdaout")
    obj_next shouldBe a [java.io.InputStream]
    val events_next = SdaOutFormat.read(obj_next).toList
    events_next.size should be (2)

  }

}
